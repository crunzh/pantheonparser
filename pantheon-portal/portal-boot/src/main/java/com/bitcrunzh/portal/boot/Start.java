package com.bitcrunzh.portal.boot;

import org.apache.felix.framework.FrameworkFactory;
import org.osgi.framework.launch.Framework;

import java.util.HashMap;
import java.util.Map;

public class Start {
    public static void main(String[] args) {
        FrameworkFactory frameworkFactory = new FrameworkFactory();
        Map<String, String> configurationMap = new HashMap<String, String>();
        Framework framework = frameworkFactory.newFramework(configurationMap);
        try {
            framework.start();
            framework.waitForStop(0);
            System.exit(0);
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("Could not create framework: " + e);
            System.exit(1);
        }
    }
}
