package com.bitcrunzh.portal.api.model;

public enum GameType {
    Pantheon,
    EverQuest,
    Unknown;

    public static GameType parse(String gameType) {
        for(GameType type : GameType.values()) {
            if(type.name().equals(gameType)) {
                return type;
            }
        }
        return Unknown;
    }
}
