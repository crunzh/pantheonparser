package com.bitcrunzh.portal.api.model.log.combat;

import java.util.UUID;

public class Kill {
    private String characterName;
    private UUID characterId;
    private String nameOfKill;
    private UUID killedMonsterId;
    private UUID killedCharacterId;
    private long timeOfKill;

    public Kill() {
    }

    public Kill(String characterName, UUID characterId, String nameOfKill, UUID killedMonsterId, UUID killedCharacterId, long timeOfKill) {
        this.characterName = characterName;
        this.characterId = characterId;
        this.nameOfKill = nameOfKill;
        this.killedMonsterId = killedMonsterId;
        this.killedCharacterId = killedCharacterId;
        this.timeOfKill = timeOfKill;
    }

    public String getCharacterName() {
        return characterName;
    }

    public void setCharacterName(String characterName) {
        this.characterName = characterName;
    }

    public UUID getCharacterId() {
        return characterId;
    }

    public void setCharacterId(UUID characterId) {
        this.characterId = characterId;
    }

    public String getNameOfKill() {
        return nameOfKill;
    }

    public void setNameOfKill(String nameOfKill) {
        this.nameOfKill = nameOfKill;
    }

    public UUID getKilledMonsterId() {
        return killedMonsterId;
    }

    public void setKilledMonsterId(UUID killedMonsterId) {
        this.killedMonsterId = killedMonsterId;
    }

    public UUID getKilledCharacterId() {
        return killedCharacterId;
    }

    public void setKilledCharacterId(UUID killedCharacterId) {
        this.killedCharacterId = killedCharacterId;
    }

    public long getTimeOfKill() {
        return timeOfKill;
    }

    public void setTimeOfKill(long timeOfKill) {
        this.timeOfKill = timeOfKill;
    }
}
