package com.bitcrunzh.portal.api.model.log.magic;

public class CastSpell {
    private long timeOfCast;
    private String spellName;
    private String casterName;
    private String targetName;
    private Integer damage;
    private String effect;
    private Float resistPct;
}
