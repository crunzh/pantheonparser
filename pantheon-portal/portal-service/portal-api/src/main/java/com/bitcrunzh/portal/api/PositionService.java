package com.bitcrunzh.portal.api;

import com.bitcrunzh.portal.api.model.log.position.PositionReport;
import com.bitcrunzh.portal.api.model.log.position.ZoneChange;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/v1/pantheon/portal/position")
public interface PositionService {

    @GET
    @Path("/positionReports/{sessionId}/{gameType}/{characterId}/{from}/{to}")
    @Produces(MediaType.APPLICATION_JSON)
    List<PositionReport> getPositionReports(@PathParam("sessionId") String sessionId, @PathParam("gameType") String gameType, @PathParam("characterId") String characterId, @PathParam("from") long from, @PathParam("to") long to);

    @GET
    @Path("/position/{sessionId}/{gameType}/{characterId}/{from}/{to}")
    @Produces(MediaType.APPLICATION_JSON)
    PositionReport getExpectedPosition(@PathParam("sessionId") String sessionId, @PathParam("gameType") String gameType, @PathParam("characterId") String characterId, @PathParam("from") long time);

    @GET
    @Path("/zoneChanges/{sessionId}/{gameType}/{characterId}/{from}/{to}")
    @Produces(MediaType.APPLICATION_JSON)
    List<ZoneChange> getZoneChanges(@PathParam("sessionId") String sessionId, @PathParam("gameType") String gameType, @PathParam("characterId") String characterId, @PathParam("from") long from, @PathParam("to") long to);

    @GET
    @Path("/zone/{sessionId}/{gameType}/{characterId}/{from}/{to}")
    @Produces(MediaType.APPLICATION_JSON)
    ZoneChange getExpectedZone(@PathParam("sessionId") String sessionId, @PathParam("gameType") String gameType, @PathParam("characterId") String characterId, @PathParam("from") long time);
}
