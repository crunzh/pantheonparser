package com.bitcrunzh.portal.api.model.log.equipment;

import java.util.UUID;

public class Item {
    private UUID itemId;
    private String itemName;
    private String gameType;
    private String iconUrl;
    private String externalReference;

    public Item() {
    }

    public Item(UUID itemId, String itemName, String gameType, String iconUrl, String externalReference) {
        this.itemId = itemId;
        this.itemName = itemName;
        this.gameType = gameType;
        this.iconUrl = iconUrl;
        this.externalReference = externalReference;
    }

    public UUID getItemId() {
        return itemId;
    }

    public void setItemId(UUID itemId) {
        this.itemId = itemId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public String getExternalReference() {
        return externalReference;
    }

    public void setExternalReference(String externalReference) {
        this.externalReference = externalReference;
    }

    public String getGameType() {
        return gameType;
    }

    public void setGameType(String gameType) {
        this.gameType = gameType;
    }
}
