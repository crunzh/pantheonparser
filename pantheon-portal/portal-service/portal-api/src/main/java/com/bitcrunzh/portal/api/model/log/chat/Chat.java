package com.bitcrunzh.portal.api.model.log.chat;

import java.util.UUID;

public class Chat {
    private String from;
    private UUID fromCharacterId;
    private String text;
    private String chatType;
    private String channelName;
    private long timeOfChat;

    public Chat() {
    }

    public Chat(String from, UUID fromCharacterId, String text, String chatType, String channelName, long timeOfChat) {
        this.from = from;
        this.fromCharacterId = fromCharacterId;
        this.text = text;
        this.chatType = chatType;
        this.channelName = channelName;
        this.timeOfChat = timeOfChat;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public UUID getFromCharacterId() {
        return fromCharacterId;
    }

    public void setFromCharacterId(UUID fromCharacterId) {
        this.fromCharacterId = fromCharacterId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getChatType() {
        return chatType;
    }

    public void setChatType(String chatType) {
        this.chatType = chatType;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public long getTimeOfChat() {
        return timeOfChat;
    }

    public void setTimeOfChat(long timeOfChat) {
        this.timeOfChat = timeOfChat;
    }
}
