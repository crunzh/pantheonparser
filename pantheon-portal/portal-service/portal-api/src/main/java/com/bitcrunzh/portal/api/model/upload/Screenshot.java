package com.bitcrunzh.portal.api.model.upload;

import java.util.UUID;

public class Screenshot {
    private UUID characterId;
    private UUID fileId;
    private long timeOfCapture;
    private int fileSize;
    private String fileType;

    public Screenshot() {
    }

    public Screenshot(UUID characterId, UUID fileId, long timeOfCapture, int fileSize, String fileType) {
        this.characterId = characterId;
        this.fileId = fileId;
        this.timeOfCapture = timeOfCapture;
        this.fileSize = fileSize;
        this.fileType = fileType;
    }

    public UUID getCharacterId() {
        return characterId;
    }

    public void setCharacterId(UUID characterId) {
        this.characterId = characterId;
    }

    public UUID getFileId() {
        return fileId;
    }

    public void setFileId(UUID fileId) {
        this.fileId = fileId;
    }

    public long getTimeOfCapture() {
        return timeOfCapture;
    }

    public void setTimeOfCapture(long timeOfCapture) {
        this.timeOfCapture = timeOfCapture;
    }

    public int getFileSize() {
        return fileSize;
    }

    public void setFileSize(int fileSize) {
        this.fileSize = fileSize;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }
}
