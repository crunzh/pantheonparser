package com.bitcrunzh.portal.api.model.log.advancement;

import java.util.UUID;

public class LevelUp {
    private String characterName;
    private UUID characterId;
    private int newLevel;
    private long timeOfLevelUp;

    public LevelUp() {
    }

    public LevelUp(String characterName, UUID characterId, int newLevel, long timeOfLevelUp) {
        this.characterName = characterName;
        this.characterId = characterId;
        this.newLevel = newLevel;
        this.timeOfLevelUp = timeOfLevelUp;
    }

    public String getCharacterName() {
        return characterName;
    }

    public void setCharacterName(String characterName) {
        this.characterName = characterName;
    }

    public UUID getCharacterId() {
        return characterId;
    }

    public void setCharacterId(UUID characterId) {
        this.characterId = characterId;
    }

    public int getNewLevel() {
        return newLevel;
    }

    public void setNewLevel(int newLevel) {
        this.newLevel = newLevel;
    }

    public long getTimeOfLevelUp() {
        return timeOfLevelUp;
    }

    public void setTimeOfLevelUp(long timeOfLevelUp) {
        this.timeOfLevelUp = timeOfLevelUp;
    }
}
