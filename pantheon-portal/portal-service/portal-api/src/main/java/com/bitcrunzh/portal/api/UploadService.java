package com.bitcrunzh.portal.api;

import com.bitcrunzh.portal.api.model.user.SessionExpiredException;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;

@Path("/v1/pantheon/portal/upload")
public interface UploadService {

    @POST
    @Path("/log/file/{sessionId}/{gameType}/{characterId}")
    @Consumes(MediaType.APPLICATION_OCTET_STREAM)
    @Produces(MediaType.APPLICATION_JSON)
    void uploadLogFile(@PathParam("sessionId") String sessionId, @PathParam("characterId") String characterId, InputStream logFile) throws ParseException, SessionExpiredException, IOException;

    @POST
    @Path("/log/line/{sessionId}/{gameType}/{characterId}")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.APPLICATION_JSON)
    void uploadLogLine(@PathParam("sessionId") String sessionId, @PathParam("characterId") String characterId, String logLine) throws ParseException, SessionExpiredException;

    @POST
    @Path("/image/{sessionId}/{gameType}/{characterId}/{lastModified}")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    void uploadImage(@PathParam("sessionId") String sessionId,  @PathParam("characterId") String characterId, @PathParam("lastModified") long lastModified, InputStream image) throws SessionExpiredException, IOException;
}
