package com.bitcrunzh.portal.api;

import com.bitcrunzh.portal.api.model.GameCharacter;

import java.util.List;

public interface CharacterService {
    List<GameCharacter> getCharacters();
}
