package com.bitcrunzh.portal.api.model.upload;

import java.util.UUID;

public class LogFile {
    private UUID accountId;
    private String nameOfFile;
    private long timeOfUpload;
}
