package com.bitcrunzh.portal.api.model.log.chat;

import java.util.UUID;

public class Tell {
    private String from;
    private UUID fromCharacterId;
    private String to;
    private UUID toCharacterId;
    private String text;
    private long timeOfTell;

    public Tell() {
    }

    public Tell(String from, UUID fromCharacterId, String to, UUID toCharacterId, String text, long timeOfTell) {
        this.from = from;
        this.fromCharacterId = fromCharacterId;
        this.to = to;
        this.toCharacterId = toCharacterId;
        this.text = text;
        this.timeOfTell = timeOfTell;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public long getTimeOfTell() {
        return timeOfTell;
    }

    public void setTimeOfTell(long timeOfTell) {
        this.timeOfTell = timeOfTell;
    }
}
