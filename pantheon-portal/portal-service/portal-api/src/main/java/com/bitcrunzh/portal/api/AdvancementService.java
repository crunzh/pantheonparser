package com.bitcrunzh.portal.api;

import com.bitcrunzh.portal.api.model.log.advancement.LevelUp;
import com.bitcrunzh.portal.api.model.log.advancement.SkillAdvancement;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/v1/pantheon/portal/advancement")
public interface AdvancementService {

    @GET
    @Path("/lvlUps/{sessionId}/{gameType}/{characterId}")
    @Produces(MediaType.APPLICATION_JSON)
    List<LevelUp> getLevelUps(@PathParam("sessionId") String sessionId, @PathParam("gameType") String gameType, @PathParam("characterId") String characterId);

    @GET
    @Path("/skillUps/{sessionId}/{gameType}/{characterId}")
    @Produces(MediaType.APPLICATION_JSON)
    List<SkillAdvancement> getSkillAdvancements(@PathParam("sessionId") String sessionId, @PathParam("gameType") String gameType, @PathParam("characterId") String characterId);
}
