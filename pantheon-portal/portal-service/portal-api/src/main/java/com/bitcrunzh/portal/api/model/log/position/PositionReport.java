package com.bitcrunzh.portal.api.model.log.position;

import java.util.UUID;

public class PositionReport {
    private String characterName;
    private UUID characterId;
    private long positionTime;
    private double xCoordinate;
    private double yCoordinate;
    private double zCoordinate;

    public PositionReport() {
    }

    public PositionReport(String characterName, UUID characterId, long positionTime, double xCoordinate, double yCoordinate, double zCoordinate) {
        this.characterName = characterName;
        this.characterId = characterId;
        this.positionTime = positionTime;
        this.xCoordinate = xCoordinate;
        this.yCoordinate = yCoordinate;
        this.zCoordinate = zCoordinate;
    }

    public String getCharacterName() {
        return characterName;
    }

    public void setCharacterName(String characterName) {
        this.characterName = characterName;
    }

    public UUID getCharacterId() {
        return characterId;
    }

    public void setCharacterId(UUID characterId) {
        this.characterId = characterId;
    }

    public long getPositionTime() {
        return positionTime;
    }

    public void setPositionTime(long positionTime) {
        this.positionTime = positionTime;
    }

    public double getxCoordinate() {
        return xCoordinate;
    }

    public void setxCoordinate(double xCoordinate) {
        this.xCoordinate = xCoordinate;
    }

    public double getyCoordinate() {
        return yCoordinate;
    }

    public void setyCoordinate(double yCoordinate) {
        this.yCoordinate = yCoordinate;
    }

    public double getzCoordinate() {
        return zCoordinate;
    }

    public void setzCoordinate(double zCoordinate) {
        this.zCoordinate = zCoordinate;
    }
}
