package com.bitcrunzh.portal.api;

import com.bitcrunzh.portal.api.model.log.combat.MeleeDamage;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/v1/pantheon/portal/statistics")
public interface StatisticsService {

    @POST
    @Path("/dps/{accountId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    Response addDamageReport(@PathParam("accountId") String accountId, List<MeleeDamage> meleeDamageReports);

    @GET
    @Path("/dps/point/{accountId}/{damageGiver}")
    @Produces(MediaType.APPLICATION_JSON)
    Response getDpsPointStatistic(@PathParam("accountId") String accountId, @PathParam("damageGiver") String damageGiverName, @QueryParam("statisticPeriodMs") int statisticPeriodMs);

    @GET
    @Path("/dps/point/{accountId}/{damageGiver}/{damageTaker}")
    @Produces(MediaType.APPLICATION_JSON)
    Response getDpsPointStatisticForTarget(@PathParam("accountId") String accountId, @PathParam("damageGiver") String damageGiverName, @PathParam("damageTaker") String damageTakerName, @QueryParam("statisticPeriodMs") int statisticPeriodMs);

    @GET
    @Path("/dps/graph/{accountId}/{damageGiver}")
    @Produces(MediaType.APPLICATION_JSON)
    Response getDpsGraphStatistic(@PathParam("accountId") String accountId, @PathParam("damageGiver") String damageGiverName, @QueryParam("pointIntervalMs") int pointIntervalMs, @QueryParam("historyLengthMs") int historyLengthMs);

    @GET
    @Path("/dps/graph/{accountId}/{damageGiver}/{damageTaker}")
    @Produces(MediaType.APPLICATION_JSON)
    Response getDpsGraphStatisticForTarget(@PathParam("accountId") String accountId, @PathParam("damageGiver") String damageGiverName, @PathParam("damageTaker") String damageTakerName, @QueryParam("pointIntervalMs") int pointIntervalMs, @QueryParam("historyLengthMs") int historyLengthMs);
}
