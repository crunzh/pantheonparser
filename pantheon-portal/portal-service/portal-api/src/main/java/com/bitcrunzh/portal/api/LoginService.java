package com.bitcrunzh.portal.api;

import com.bitcrunzh.portal.api.model.GameCharacter;
import com.bitcrunzh.portal.api.model.GameType;
import com.bitcrunzh.portal.api.model.user.CharacterAlreadyExistsException;
import com.bitcrunzh.portal.api.model.user.LoginException;
import com.bitcrunzh.portal.api.model.user.Session;
import com.bitcrunzh.portal.api.model.user.SessionExpiredException;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

//TODO Switch to OAuth + JWT
@Path("/v1/pantheon/portal/user")
public interface LoginService {

    /**
     * Login to the portal, returning a session if user is authenticated.
     * @param email to login
     * @param password matching user
     * @return
     */
    @POST
    @Path("/login/{email}")
    @Produces(MediaType.TEXT_PLAIN)
    @Consumes(MediaType.TEXT_PLAIN)
    Session login(@PathParam("email") String email, String password) throws LoginException;

    @GET
    @Path("/characters")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.APPLICATION_JSON)
    List<GameCharacter> getCharacters(String sessionId) throws SessionExpiredException;

    @POST
    @Path("/logout/{sessionId}")
    void logout(@PathParam("sessionId") String sessionId);

    @POST
    @Path("/create/{email}")
    @Consumes(MediaType.TEXT_PLAIN)
    void createAccount(@PathParam("email") String email, String password);

    @GET
    @Path("/validate/email/exists/{email}")
    @Produces(MediaType.TEXT_PLAIN)
    boolean accountExists(@PathParam("email") String email);

    @GET
    @Path("/validate/password}")
    @Consumes(MediaType.TEXT_PLAIN)
    void validatePasswordComplexity(String password);

    /**
     * Create a new character for the account matching the given session.
     * It is not allowed to assign values to characterId or ownerAccountId.
     * @param sessionId to create character for
     * @param characterToCreate to create
     * @return the created character with ids set.
     * @throws SessionExpiredException if session has expired.
     * @throws CharacterAlreadyExistsException if character already exists.
     */
    @POST
    @Path("character/{sessionId}")
    @Consumes(MediaType.APPLICATION_JSON)
    GameCharacter createCharacter(String sessionId, GameCharacter characterToCreate) throws SessionExpiredException, CharacterAlreadyExistsException;

    //TODO create a way to recover password.

}
