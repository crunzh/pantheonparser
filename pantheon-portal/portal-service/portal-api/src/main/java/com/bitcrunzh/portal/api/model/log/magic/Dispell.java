package com.bitcrunzh.portal.api.model.log.magic;

import java.util.List;

public class Dispell {
    private long timeOfDispell;
    private List<String> effectsDispelled;
    private String dispellCaster;
    private String dispellTarget;
}
