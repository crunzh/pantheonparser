package com.bitcrunzh.portal.api.model.user;

import java.util.UUID;

public class Session {
    private UUID accountId;
    private String sessionId;
    private long sessionStartTime;
    private long lastSessionRefresh;
    private int sessionTimeoutInterval;

    public Session() {
    }

    public Session(UUID accountId, String sessionId, long sessionStartTime, long lastSessionRefresh, int sessionTimeoutInterval) {
        this.accountId = accountId;
        this.sessionId = sessionId;
        this.sessionStartTime = sessionStartTime;
        this.lastSessionRefresh = lastSessionRefresh;
        this.sessionTimeoutInterval = sessionTimeoutInterval;
    }

    public UUID getAccountId() {
        return accountId;
    }

    public void setAccountId(UUID accountId) {
        this.accountId = accountId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public long getSessionStartTime() {
        return sessionStartTime;
    }

    public void setSessionStartTime(long sessionStartTime) {
        this.sessionStartTime = sessionStartTime;
    }

    public long getLastSessionRefresh() {
        return lastSessionRefresh;
    }

    public void setLastSessionRefresh(long lastSessionRefresh) {
        this.lastSessionRefresh = lastSessionRefresh;
    }

    public int getSessionTimeoutInterval() {
        return sessionTimeoutInterval;
    }

    public void setSessionTimeoutInterval(int sessionTimeoutInterval) {
        this.sessionTimeoutInterval = sessionTimeoutInterval;
    }

    public boolean isSessionTimedOut(long currentTime) {
        return lastSessionRefresh + sessionTimeoutInterval < currentTime;
    }
}
