package com.bitcrunzh.portal.api.model;

import java.util.UUID;

public class GameCharacter {
    private UUID characterId;
    private UUID ownerAccountId;
    private String serverName;
    private String characterName;
    private String characterClass;
    private GameType gameType;

    public GameCharacter() {
    }

    public GameCharacter(UUID characterId, UUID ownerAccountId, String serverName, String characterName, String characterClass, GameType gameType) {
        this.characterId = characterId;
        this.ownerAccountId = ownerAccountId;
        this.serverName = serverName;
        this.characterName = characterName;
        this.characterClass = characterClass;
        this.gameType = gameType;
    }

    public UUID getCharacterId() {
        return characterId;
    }

    public void setCharacterId(UUID characterId) {
        this.characterId = characterId;
    }

    public UUID getOwnerAccountId() {
        return ownerAccountId;
    }

    public void setOwnerAccountId(UUID ownerAccountId) {
        this.ownerAccountId = ownerAccountId;
    }

    public String getServerName() {
        return serverName;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    public String getCharacterName() {
        return characterName;
    }

    public void setCharacterName(String characterName) {
        this.characterName = characterName;
    }

    public String getCharacterClass() {
        return characterClass;
    }

    public void setCharacterClass(String characterClass) {
        this.characterClass = characterClass;
    }

    public GameType getGameType() {
        return gameType;
    }

    public void setGameType(GameType gameType) {
        this.gameType = gameType;
    }
}
