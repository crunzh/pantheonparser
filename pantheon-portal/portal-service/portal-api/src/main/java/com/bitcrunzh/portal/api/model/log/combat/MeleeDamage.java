package com.bitcrunzh.portal.api.model.log.combat;

import java.util.UUID;

public class MeleeDamage {
    private long dmgTime;
    private String damageGiverName;
    private UUID damageGiverCharacterId;
    private UUID damageGiverMonsterId;
    private String damageTakerName;
    private UUID damageTakerCharacterId;
    private UUID damageTakerMonsterId;
    private String damageType;
    private int amount;
    private Float resistPct;

    public MeleeDamage() {
    }

    public MeleeDamage(long dmgTime, String damageGiverName, UUID damageGiverCharacterId, UUID damageGiverMonsterId, String damageTakerName, UUID damageTakerCharacterId, UUID damageTakerMonsterId, String damageType, int amount, Float resistPct) {
        this.dmgTime = dmgTime;
        this.damageGiverName = damageGiverName;
        this.damageGiverCharacterId = damageGiverCharacterId;
        this.damageGiverMonsterId = damageGiverMonsterId;
        this.damageTakerName = damageTakerName;
        this.damageTakerCharacterId = damageTakerCharacterId;
        this.damageTakerMonsterId = damageTakerMonsterId;
        this.damageType = damageType;
        this.amount = amount;
        this.resistPct = resistPct;
    }

    public long getDmgTime() {
        return dmgTime;
    }

    public void setDmgTime(long dmgTime) {
        this.dmgTime = dmgTime;
    }

    public String getDamageGiverName() {
        return damageGiverName;
    }

    public void setDamageGiverName(String damageGiverName) {
        this.damageGiverName = damageGiverName;
    }

    public UUID getDamageGiverCharacterId() {
        return damageGiverCharacterId;
    }

    public void setDamageGiverCharacterId(UUID damageGiverCharacterId) {
        this.damageGiverCharacterId = damageGiverCharacterId;
    }

    public UUID getDamageGiverMonsterId() {
        return damageGiverMonsterId;
    }

    public void setDamageGiverMonsterId(UUID damageGiverMonsterId) {
        this.damageGiverMonsterId = damageGiverMonsterId;
    }

    public String getDamageTakerName() {
        return damageTakerName;
    }

    public void setDamageTakerName(String damageTakerName) {
        this.damageTakerName = damageTakerName;
    }

    public UUID getDamageTakerCharacterId() {
        return damageTakerCharacterId;
    }

    public void setDamageTakerCharacterId(UUID damageTakerCharacterId) {
        this.damageTakerCharacterId = damageTakerCharacterId;
    }

    public UUID getDamageTakerMonsterId() {
        return damageTakerMonsterId;
    }

    public void setDamageTakerMonsterId(UUID damageTakerMonsterId) {
        this.damageTakerMonsterId = damageTakerMonsterId;
    }

    public String getDamageType() {
        return damageType;
    }

    public void setDamageType(String damageType) {
        this.damageType = damageType;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Float getResistPct() {
        return resistPct;
    }

    public void setResistPct(Float resistPct) {
        this.resistPct = resistPct;
    }
}
