package com.bitcrunzh.portal.api.model.log.position;

import java.util.UUID;

public class ZoneChange {
    private String characterName;
    private UUID characterId;
    private long zoneTime;
    private String newZone;

    public ZoneChange() {
    }

    public ZoneChange(String characterName, UUID characterId, long zoneTime, String newZone) {
        this.characterName = characterName;
        this.characterId = characterId;
        this.zoneTime = zoneTime;
        this.newZone = newZone;
    }

    public String getCharacterName() {
        return characterName;
    }

    public void setCharacterName(String characterName) {
        this.characterName = characterName;
    }

    public UUID getCharacterId() {
        return characterId;
    }

    public void setCharacterId(UUID characterId) {
        this.characterId = characterId;
    }

    public long getZoneTime() {
        return zoneTime;
    }

    public void setZoneTime(long zoneTime) {
        this.zoneTime = zoneTime;
    }

    public String getNewZone() {
        return newZone;
    }

    public void setNewZone(String newZone) {
        this.newZone = newZone;
    }
}
