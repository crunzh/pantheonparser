package com.bitcrunzh.portal.api.model.log.advancement;

import java.util.UUID;

public class SkillAdvancement {
    private String characterName;
    private UUID characterId;
    private String skillName;
    private int newSkillLevel;
    private long timeOfAdvancement;

    public SkillAdvancement() {
    }

    public SkillAdvancement(String characterName, UUID characterId, String skillName, int newSkillLevel, long timeOfAdvancement) {
        this.characterName = characterName;
        this.characterId = characterId;
        this.skillName = skillName;
        this.newSkillLevel = newSkillLevel;
        this.timeOfAdvancement = timeOfAdvancement;
    }

    public String getCharacterName() {
        return characterName;
    }

    public void setCharacterName(String characterName) {
        this.characterName = characterName;
    }

    public UUID getCharacterId() {
        return characterId;
    }

    public void setCharacterId(UUID characterId) {
        this.characterId = characterId;
    }

    public String getSkillName() {
        return skillName;
    }

    public void setSkillName(String skillName) {
        this.skillName = skillName;
    }

    public int getNewSkillLevel() {
        return newSkillLevel;
    }

    public void setNewSkillLevel(int newSkillLevel) {
        this.newSkillLevel = newSkillLevel;
    }

    public long getTimeOfAdvancement() {
        return timeOfAdvancement;
    }

    public void setTimeOfAdvancement(long timeOfAdvancement) {
        this.timeOfAdvancement = timeOfAdvancement;
    }
}
