package com.bitcrunzh.portal.api.model.log.loot;

import java.util.UUID;

public class ItemLoot {
    private String characterName;
    private UUID characterId;
    private String itemName;
    private UUID itemId;
    private long lootTime;

    public ItemLoot() {
    }

    public ItemLoot(String characterName, UUID characterId, String itemName, UUID itemId, long lootTime) {
        this.characterName = characterName;
        this.characterId = characterId;
        this.itemName = itemName;
        this.itemId = itemId;
        this.lootTime = lootTime;
    }

    public String getCharacterName() {
        return characterName;
    }

    public void setCharacterName(String characterName) {
        this.characterName = characterName;
    }

    public UUID getCharacterId() {
        return characterId;
    }

    public void setCharacterId(UUID characterId) {
        this.characterId = characterId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public UUID getItemId() {
        return itemId;
    }

    public void setItemId(UUID itemId) {
        this.itemId = itemId;
    }

    public long getLootTime() {
        return lootTime;
    }

    public void setLootTime(long lootTime) {
        this.lootTime = lootTime;
    }
}
