package com.bitcrunzh.portal.internal.storage;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FileStorageImplTest {

    @Test
    void getScaleRatio() {
        //Arrange
        int max = 100;
        int height = 300;
        int width = 200;

        //Act
        double ratio = FileStorageImpl.getScaleRatio(width, height, max);
        int newHeight = (int) Math.ceil(height * ratio);
        int newWidth = (int) Math.ceil(width * ratio);

        //Assert
        assertEquals(0.333D, ratio, 0.001D);
        assertEquals(max, newHeight);
        assertEquals(67, newWidth);
    }
}