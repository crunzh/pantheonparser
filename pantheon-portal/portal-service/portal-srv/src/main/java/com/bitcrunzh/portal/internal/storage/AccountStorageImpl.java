package com.bitcrunzh.portal.internal.storage;

import com.bitcrunzh.portal.api.model.GameCharacter;
import com.bitcrunzh.portal.internal.account.Account;
import com.mongodb.client.*;
import com.mongodb.client.model.IndexOptions;
import com.mongodb.client.model.Indexes;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;
import static com.mongodb.client.model.Filters.*;

public class AccountStorageImpl implements AccountStorage {
    private static final String CHARACTER_COLLECTION = "characters";
    private boolean indexChecked = false;
    private static final String DATABASE_NAME = "PantheonPortal";
    private static final String ACCOUNT_COLLECTION = "account";
    private final MongoClient mongoClient;
    private MongoDatabase database;
    private MongoCollection<Account> accountCollection;
    private MongoCollection<GameCharacter> characterCollection;

    public AccountStorageImpl(MongoClient mongoClient) {
        this.mongoClient = mongoClient;
        setAccountIndexes();
        setCharacterIndexes();
    }

    @Override
    public Account getAccount(String email) {
        return getAccountCollection().find(eq("email", email)).first();
    }

    @Override
    public void setAccount(Account account) {
        getAccountCollection().insertOne(account);
    }

    @Override
    public void deleteAccount(UUID accountId) {
        getAccountCollection().deleteOne(eq("accountId", accountId));
        getCharacterCollection().deleteMany(eq("ownerAccountId", accountId));
    }

    @Override
    public List<GameCharacter> getCharacters(UUID accountId) {
        List<GameCharacter> characters = new ArrayList<>();
        for(GameCharacter gameCharacter : getCharacterCollection().find(eq("accountId", accountId))) {
            characters.add(gameCharacter);
        }
        return characters;
    }

    @Override
    public void setCharacter(GameCharacter character) {
        getCharacterCollection().insertOne(character);
    }

    @Override
    public void deleteCharacter(UUID characterId) {
        getCharacterCollection().deleteOne(eq("characterId", characterId));
    }

    private MongoCollection<Account> getAccountCollection() {
        if (accountCollection == null) {
            accountCollection = getDatabase().getCollection(ACCOUNT_COLLECTION, Account.class);
        }
        return accountCollection;
    }

    private MongoCollection<GameCharacter> getCharacterCollection() {
        if (characterCollection == null) {
            characterCollection = getDatabase().getCollection(CHARACTER_COLLECTION, GameCharacter.class);
        }
        return characterCollection;
    }

    private MongoDatabase getDatabase() {
        if (database == null) {
            database = mongoClient.getDatabase(DATABASE_NAME);
        }
        return database;
    }

    private void setAccountIndexes() {
        IndexOptions indexOptions = new IndexOptions().unique(true);
        getAccountCollection().createIndex(Indexes.ascending("email"), indexOptions);
    }

    private void setCharacterIndexes() {
        IndexOptions indexOptions = new IndexOptions().unique(true);
        getAccountCollection().createIndex(Indexes.ascending("characterId"), indexOptions);
        getAccountCollection().createIndex(Indexes.ascending("ownerAccountId"));
    }
}
