package com.bitcrunzh.portal.internal.storage;

import com.bitcrunzh.portal.internal.storage.model.FileSaveResult;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public interface FileStorage {
    FileSaveResult saveFile(InputStream inputStream) throws IOException;
    FileSaveResult saveImage(InputStream inputStream) throws IOException;
    FileSaveResult saveImage(InputStream inputStream, int maxHeightOrWidthPx) throws IOException;

    OutputStream readFile(String fileId) throws FileNotFoundException;
}
