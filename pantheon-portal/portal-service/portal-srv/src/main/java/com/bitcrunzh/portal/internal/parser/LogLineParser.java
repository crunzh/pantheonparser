package com.bitcrunzh.portal.internal.parser;

import com.bitcrunzh.portal.api.model.GameCharacter;
import com.bitcrunzh.portal.internal.storage.ModelStorage;

import java.text.ParseException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Creates log events matching a specific log line
 */
public interface LogLineParser {

    boolean isMultiSentenceLine();

    Pattern getPattern();

    /**
     *
     * @param matcher to parse
     * @return object if log line matches.
     * @throws ParseException if match but unexpected parse problem occurs.
     */
    void consume(Matcher matcher, GameCharacter character, ParserVisitor visitor) throws ParseException;
}
