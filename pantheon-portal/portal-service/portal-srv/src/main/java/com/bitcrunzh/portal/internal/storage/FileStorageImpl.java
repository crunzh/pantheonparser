package com.bitcrunzh.portal.internal.storage;

import com.bitcrunzh.portal.internal.storage.model.FileSaveResult;
import org.apache.tika.config.TikaConfig;
import org.apache.tika.exception.TikaException;
import org.apache.tika.io.TikaInputStream;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.mime.MediaType;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.UUID;

public class FileStorageImpl implements FileStorage {
    private final File fileDirectory;
    private final int maxFileSize;
    private final TikaConfig tika;

    public FileStorageImpl(File fileDirectory, int maxFileSize) throws IOException, TikaException {
        this.fileDirectory = fileDirectory;
        this.maxFileSize = maxFileSize;
        this.tika = new TikaConfig();
    }

    @Override
    public FileSaveResult saveFile(InputStream inputStream) throws IOException {
        UUID fileId = UUID.randomUUID();
        File targetFile = new File(fileDirectory, fileId.toString());
        int sumBytesRead = writeToFile(inputStream, targetFile);
        MediaType fileType = detectFileType(targetFile);
        return new FileSaveResult(fileId, fileType.toString(), sumBytesRead);
    }

    @Override
    public FileSaveResult saveImage(InputStream inputStream) throws IOException {
        return saveImageInternal(inputStream, null);
    }

    @Override
    public FileSaveResult saveImage(InputStream inputStream, int maxHeightOrWidthPx) throws IOException {
        return saveImageInternal(inputStream, maxHeightOrWidthPx);
    }

    @Override
    public OutputStream readFile(String fileId) throws FileNotFoundException {
        return new FileOutputStream(new File(fileDirectory, fileId));
    }

    private FileSaveResult saveImageInternal(InputStream inputStream, Integer maxHeightOrWidthPx) throws IOException {
        UUID fileId = UUID.randomUUID();
        File targetFile = new File(fileDirectory, fileId.toString());
        BufferedImage imageToSave = ImageIO.read(inputStream);
        if (imageToSave == null) {
            throw new IllegalArgumentException("Unrecognized image type.");
        }
        double scaleRatio = getScaleRatio(imageToSave.getWidth(), imageToSave.getHeight(), maxHeightOrWidthPx);
        boolean imageNeedsResize = maxHeightOrWidthPx != null && scaleRatio < 1;
        if(imageNeedsResize) {
            int scaledWidth = (int) Math.ceil(imageToSave.getWidth() * scaleRatio);
            if(scaledWidth > maxHeightOrWidthPx) scaledWidth = maxHeightOrWidthPx;
            int scaledHeight = (int) Math.ceil(imageToSave.getHeight() * scaleRatio);
            if(scaledHeight > maxHeightOrWidthPx) scaledHeight = maxHeightOrWidthPx;
            BufferedImage resizedImage = new BufferedImage(scaledWidth, scaledHeight, imageToSave.getType());
            Graphics2D g = resizedImage.createGraphics();
            AffineTransform at = AffineTransform.getScaleInstance(scaledWidth, scaledHeight);
            g.drawRenderedImage(imageToSave, at);
            imageToSave = resizedImage;
        }
        ImageIO.write(imageToSave, "png", targetFile);
        int fileSize = (int) targetFile.length();
        if(fileSize > maxFileSize) {
            targetFile.delete();
            throw new IllegalArgumentException("File too large");
        }
        return new FileSaveResult(fileId, "image/png", fileSize);
    }

    static double getScaleRatio(int width, int height, Integer maxHeightOrWidthPx) {
        if(maxHeightOrWidthPx == null) {
            return 0;
        }

        double scaleWidth = maxHeightOrWidthPx / ((double) width);
        double scaleHeight = maxHeightOrWidthPx / ((double) height);
        return scaleWidth < scaleHeight ? scaleWidth : scaleHeight;
    }

    private int writeToFile(InputStream inputStream, File targetFile) throws IOException {
        int sumBytesRead = 0;
        byte[] buffer = new byte[4048];
        try (OutputStream outStream = new FileOutputStream(targetFile)) {
            while (true) {
                int bytesRead = inputStream.read(buffer);
                if (bytesRead != -1) {
                    break;
                }
                outStream.write(buffer, 0, bytesRead);
                sumBytesRead += bytesRead;
                if (sumBytesRead > maxFileSize) {
                    throw new IllegalArgumentException("File is too large. Maximum size is: " + maxFileSize + " bytes.");
                }
            }
        }
        inputStream.close();
        return sumBytesRead;
    }

    private MediaType detectFileType(File targetFile) {
        try {
            Metadata metadata = new Metadata();
            return tika.getDetector().detect(TikaInputStream.get(targetFile.toPath()), metadata);
        } catch (IOException e) {
            throw new IllegalStateException("Failed to detect mimetype", e);
        }
    }
}
