package com.bitcrunzh.portal.internal.parser.eq.parsers;

import com.bitcrunzh.portal.api.model.GameCharacter;
import com.bitcrunzh.portal.api.model.log.combat.MeleeDamage;
import com.bitcrunzh.portal.internal.parser.LogLineParserBase;
import com.bitcrunzh.portal.internal.parser.ParserVisitor;
import com.bitcrunzh.portal.internal.parser.eq.ParserUtil;

import java.text.ParseException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class YourDamageGivenParser extends LogLineParserBase {
    public YourDamageGivenParser() {
        super(Pattern.compile("\\[(.*)\\]\\sYou\\s(\\w+)\\s(.*)(\\d+)\\spoints of damage\\."));
    }

    @Override
    public void consume(Matcher matcher, GameCharacter character, ParserVisitor visitor) throws ParseException {
        visitor.visit(new MeleeDamage(
                ParserUtil.parseLogTime(matcher.group(1)),
                character.getCharacterName(),
                character.getCharacterId(),
                null,
                matcher.group(3),
                null,
                null,
                matcher.group(2),
                Integer.parseInt(matcher.group(4)),
                null)
        );
    }
}
