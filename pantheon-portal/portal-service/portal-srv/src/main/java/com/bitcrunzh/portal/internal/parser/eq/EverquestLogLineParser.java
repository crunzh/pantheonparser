package com.bitcrunzh.portal.internal.parser.eq;

import com.bitcrunzh.portal.api.model.GameCharacter;
import com.bitcrunzh.portal.internal.parser.LogLineParser;
import com.bitcrunzh.portal.internal.parser.ParserVisitor;
import com.bitcrunzh.portal.internal.storage.ModelStorage;
import com.sun.org.apache.xerces.internal.impl.xpath.regex.Match;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;

public class EverquestLogLineParser {
    private static final int MAX_LOG_LINE_LENGTH = 1000;
    private final List<LogLineParser> singleEventParsers = new ArrayList<>(); //These parser only
    private final List<LogLineParser> multiEventParsers = new ArrayList<>(); //Parsing of lines which may be accessed by multiple parsers.


    public EverquestLogLineParser() {
        //Add all parsers in significant order compared to likelihood of parser match.
        //singleEventParsers.add();

    }

    public void consume(String logInput, GameCharacter character, ParserVisitor visitor) throws ParseException {
        if(logInput == null || logInput.length() < 3) {
            return;
        }
        for(String logLine : logInput.split("\\r?\\n")) {
            if(logLine.length() > MAX_LOG_LINE_LENGTH) {
                throw new IllegalArgumentException("Log line was longer than the maximum supported "+MAX_LOG_LINE_LENGTH+" characters.");
            }
            parseLine(logLine, character, visitor);
        }
    }

    private void parseLine(String logLine, GameCharacter character, ParserVisitor visitor) throws ParseException {
        for(LogLineParser parser : singleEventParsers) {
            Matcher matcher = parser.getPattern().matcher(logLine);
            if (matcher.matches()) {
                parser.consume(matcher, character, visitor);
                return;
            }
        }
        for(LogLineParser parser : multiEventParsers) {
            Matcher matcher = parser.getPattern().matcher(logLine);
            if (matcher.matches()) {
                parser.consume(matcher, character, visitor);
            }
        }
    }
}
