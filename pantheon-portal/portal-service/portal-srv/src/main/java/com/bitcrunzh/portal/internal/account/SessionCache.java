package com.bitcrunzh.portal.internal.account;


import com.bitcrunzh.portal.api.model.user.SessionExpiredException;

import javax.security.auth.login.LoginException;

public interface SessionCache {
    UserSession refreshSession(String sessionId) throws SessionExpiredException;
    void authorizeSession(String sessionId) throws SessionExpiredException;
    void logout(String sessionId);
    UserSession login(String email, String password) throws LoginException;
    UserSession login(Account account);
}
