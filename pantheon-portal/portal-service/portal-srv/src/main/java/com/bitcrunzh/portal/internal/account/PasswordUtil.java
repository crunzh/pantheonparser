package com.bitcrunzh.portal.internal.account;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

public class PasswordUtil {

    private static final int MIN_PASSWORD_LENGTH = 8;

    public static byte[] getPasswordHash(String password, byte[] salt) {
        MessageDigest digest;
        try {
            digest = MessageDigest.getInstance("SHA-512");
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalStateException("SHA-512 not present.", e);
        }
        digest.update(salt);
        digest.update(password.getBytes(StandardCharsets.UTF_8));
        return digest.digest();
    }

    public static byte[] createSalt() {
        SecureRandom secureRandom = new SecureRandom();
        byte[] newSalt = new byte[64];
        secureRandom.nextBytes(newSalt);
        return newSalt;
    }

    public static void assertPasswordComplexity(String password) {
        if(password == null) {
            throw new IllegalArgumentException("Password cannot be null");
        }
        if(password.length() < MIN_PASSWORD_LENGTH) {
            throw new IllegalArgumentException("Password must be longer than "+MIN_PASSWORD_LENGTH);
        }
        //TODO add more complexity?
    }
}
