package com.bitcrunzh.portal.internal.services;

import com.bitcrunzh.portal.api.UploadService;
import com.bitcrunzh.portal.api.model.GameCharacter;
import com.bitcrunzh.portal.api.model.upload.Screenshot;
import com.bitcrunzh.portal.api.model.user.SessionExpiredException;
import com.bitcrunzh.portal.internal.EventHandlerFactory;
import com.bitcrunzh.portal.internal.account.SessionCache;
import com.bitcrunzh.portal.internal.account.UserSession;
import com.bitcrunzh.portal.internal.parser.eq.EverquestLogLineParser;
import com.bitcrunzh.portal.internal.storage.FileStorage;
import com.bitcrunzh.portal.internal.storage.ModelStorage;
import com.bitcrunzh.portal.internal.storage.model.FileSaveResult;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;

public class UploadServiceImpl implements UploadService {
    private static final int MAX_HEIGHT_OR_WIDTH_PX = 1024; //TODO make configurable
    private final EverquestLogLineParser eqParser = new EverquestLogLineParser();
    private final EventHandlerFactory ehFactory;
    private final SessionCache sessionCache;
    private final ModelStorage modelStorage;
    private final FileStorage fileStorage;

    public UploadServiceImpl(EventHandlerFactory ehFactory, SessionCache sessionCache, ModelStorage modelStorage, FileStorage fileStorage) {
        this.ehFactory = ehFactory;
        this.sessionCache = sessionCache;
        this.modelStorage = modelStorage;
        this.fileStorage = fileStorage;
    }

    @Override
    public void uploadLogFile(String sessionId, String characterId, InputStream logFile) throws ParseException, SessionExpiredException, IOException {
        UserSession session = sessionCache.refreshSession(sessionId);
        try (BufferedReader br = new BufferedReader(new InputStreamReader(logFile))) {
            String logLine = br.readLine();
            while (logLine != null) {
                uploadLogLine(session, characterId, logLine);
                logLine = br.readLine();
            }
        }
    }

    @Override
    public void uploadLogLine(String sessionId, String characterId, String logLine) throws ParseException, SessionExpiredException {
        UserSession session = sessionCache.refreshSession(sessionId);
        uploadLogLine(session, characterId, logLine);
    }

    private void uploadLogLine(UserSession session, String characterId, String logLine) throws ParseException {
        GameCharacter gameCharacter = getCharacter(session, characterId);
        switch (gameCharacter.getGameType()) {
            case Pantheon:
                break;
            case EverQuest:
                eqParser.consume(logLine, gameCharacter, ehFactory.create(session, gameCharacter));
                break;
        }
    }

    @Override
    public void uploadImage(String sessionId, String characterId, long lastModified, InputStream image) throws SessionExpiredException, IOException {
        UserSession session = sessionCache.refreshSession(sessionId);
        GameCharacter gameCharacter = getCharacter(session, characterId);
        FileSaveResult fileSaveResult = fileStorage.saveImage(image, MAX_HEIGHT_OR_WIDTH_PX);
        modelStorage.getCharacterStorage().addScreenshot(new Screenshot(gameCharacter.getCharacterId(), fileSaveResult.getFileID(), lastModified, fileSaveResult.getFileSize(), fileSaveResult.getFileType()));
    }

    private GameCharacter getCharacter(UserSession session, String characterId) {
        for (GameCharacter character : session.getAccountCharacters()) {
            if (character.getCharacterId().toString().equals(characterId)) {
                return character;
            }
        }
        throw new IllegalArgumentException("CharacterId provided '" + characterId + "' cannot be managed by this account.");
    }
}
