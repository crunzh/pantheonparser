package com.bitcrunzh.portal.internal.parser.eq.parsers;

import com.bitcrunzh.portal.api.model.GameCharacter;
import com.bitcrunzh.portal.api.model.log.position.PositionReport;
import com.bitcrunzh.portal.internal.parser.LogLineParserBase;
import com.bitcrunzh.portal.internal.parser.ParserVisitor;
import com.bitcrunzh.portal.internal.parser.eq.ParserUtil;
import com.bitcrunzh.portal.internal.storage.ModelStorage;

import java.text.ParseException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by RedDevil on 10-05-2017.
 */
public class LocationParser extends LogLineParserBase {
    public LocationParser() {
        super(Pattern.compile("\\[(.*)]\\sYour Location is\\s(.+),\\s(.+),\\s(.+)"));
    }

    @Override
    public void consume(Matcher matcher, GameCharacter character, ParserVisitor visitor) throws ParseException {
        visitor.visit(
                new PositionReport(
                        character.getCharacterName(),
                        character.getCharacterId(),
                        ParserUtil.parseLogTime(matcher.group(1)),
                        Double.parseDouble(matcher.group(2)),
                        Double.parseDouble(matcher.group(3)),
                        Double.parseDouble(matcher.group(4))
                ));
    }
}
