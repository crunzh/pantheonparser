package com.bitcrunzh.portal.internal.storage;

import com.mongodb.MongoClientSettings;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;

import java.util.Arrays;

import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

public class MongoModelStorageImpl implements ModelStorage {
    private final MongoClient mongoClient;
    private final AccountStorage accountStorage;

    public MongoModelStorageImpl(ServerAddress ... mongoServers) {
        CodecRegistry pojoCodecRegistry = fromRegistries(fromProviders(PojoCodecProvider.builder().automatic(true).build()));
        MongoClientSettings settings = MongoClientSettings.builder()
                .codecRegistry(pojoCodecRegistry)
                .applicationName("Pantheon Portal")
                .applyToClusterSettings(builder -> builder.hosts(Arrays.asList(mongoServers)))
                .build();
        mongoClient = MongoClients.create(settings);
        accountStorage = new AccountStorageImpl(mongoClient);
    }

    @Override
    public AccountStorage getAccountStorage() {
        return accountStorage;
    }
}
