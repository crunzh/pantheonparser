package com.bitcrunzh.portal.internal.services.exceptions;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import java.io.IOException;

public class IOExceptionMapper implements ExceptionMapper<IOException> {
    @Override
    public Response toResponse(IOException e) {
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("An error occurred processing the request").build();
    }
}
