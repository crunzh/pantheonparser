package com.bitcrunzh.portal.internal.services;

import com.bitcrunzh.portal.api.LoginService;
import com.bitcrunzh.portal.api.model.GameCharacter;
import com.bitcrunzh.portal.api.model.GameType;
import com.bitcrunzh.portal.api.model.user.CharacterAlreadyExistsException;
import com.bitcrunzh.portal.api.model.user.LoginException;
import com.bitcrunzh.portal.api.model.user.Session;
import com.bitcrunzh.portal.api.model.user.SessionExpiredException;
import com.bitcrunzh.portal.internal.account.PasswordUtil;
import com.bitcrunzh.portal.internal.account.SessionCache;
import com.bitcrunzh.portal.internal.account.UserSession;
import com.bitcrunzh.portal.internal.storage.AccountStorage;
import com.bitcrunzh.portal.internal.account.Account;

import java.util.List;

import static com.bitcrunzh.portal.internal.account.PasswordUtil.assertPasswordComplexity;

public class LoginServiceImpl implements LoginService {
    private final AccountStorage accountStorage;
    private final SessionCache sessionCache;

    public LoginServiceImpl(AccountStorage accountStorage, SessionCache sessionCache) {
        this.accountStorage = accountStorage;
        this.sessionCache = sessionCache;
    }

    @Override
    public Session login(String email, String password) throws LoginException {
        try {
            UserSession userSession = sessionCache.login(email, password);
            return new Session(userSession.getAccountId(), userSession.getSessionId(), userSession.getSessionStartTime(), userSession.getLastSessionRefresh(), userSession.getSessionTimeoutInterval());
        } catch (javax.security.auth.login.LoginException e) {
            throw new LoginException();
        }
    }

    @Override
    public List<GameCharacter> getCharacters(String sessionId) throws SessionExpiredException {
        //TODO check if account can be accessed by session.
        UserSession session = sessionCache.refreshSession(sessionId);
        return session.getAccountCharacters();
    }

    @Override
    public void logout(String sessionId) {
        sessionCache.logout(sessionId);
    }

    @Override
    public void createAccount(String email, String password) {
        if (accountExists(email)) {
            throw new IllegalArgumentException("account already exists for email.");
        }
        assertPasswordComplexity(password);
        byte[] salt = PasswordUtil.createSalt();
        Account newAccount = new Account(email, PasswordUtil.getPasswordHash(password, salt), salt);
        accountStorage.setAccount(newAccount);
        sessionCache.login(newAccount);
    }

    @Override
    public boolean accountExists(String email) {
        Account account = accountStorage.getAccount(email);
        return account != null;
    }

    @Override
    public void validatePasswordComplexity(String password) {
        assertPasswordComplexity(password);
    }

    @Override
    public GameCharacter createCharacter(String sessionId, GameCharacter characterToCreate) throws SessionExpiredException, CharacterAlreadyExistsException {
        return null;
    }


}
