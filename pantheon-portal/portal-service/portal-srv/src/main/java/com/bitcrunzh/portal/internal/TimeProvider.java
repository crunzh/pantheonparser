package com.bitcrunzh.portal.internal;

public interface TimeProvider {
    long getCurrentTimeEpoch();
}
