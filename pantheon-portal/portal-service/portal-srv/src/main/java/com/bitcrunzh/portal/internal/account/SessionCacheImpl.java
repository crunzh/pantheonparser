package com.bitcrunzh.portal.internal.account;

import com.bitcrunzh.portal.api.model.GameCharacter;
import com.bitcrunzh.portal.api.model.user.SessionExpiredException;
import com.bitcrunzh.portal.internal.TimeProvider;
import com.bitcrunzh.portal.internal.storage.AccountStorage;

import javax.security.auth.login.LoginException;
import java.util.*;

public class SessionCacheImpl implements SessionCache {
    private final Map<String, UserSession> sessionMap = new HashMap<>();
    private final Map<UUID, String> accountIdSessionIdMap = new HashMap<>();
    private final AccountStorage accountStorage;
    private final TimeProvider timeProvider;
    private final int sessionTimeoutInterval;

    public SessionCacheImpl(AccountStorage accountStorage, TimeProvider timeProvider, int sessionTimeoutInterval) {
        this.accountStorage = accountStorage;
        this.timeProvider = timeProvider;
        this.sessionTimeoutInterval = sessionTimeoutInterval;
    }

    @Override
    public UserSession refreshSession(String sessionId) throws SessionExpiredException {
        UserSession session = sessionMap.get(sessionId);

        if (!isValidSession(session)) {
            throw new SessionExpiredException();
        }
        session.setLastSessionRefresh(timeProvider.getCurrentTimeEpoch());
        return session;
    }

    @Override
    public void authorizeSession(String sessionId) throws SessionExpiredException {
        UserSession session = sessionMap.get(sessionId);

        if (!isValidSession(session)) {
            throw new SessionExpiredException();
        }
    }

    @Override
    public void logout(String sessionId) {
        synchronized (sessionMap) {
            UserSession session = sessionMap.remove(sessionId);
            if (session != null) {
                accountIdSessionIdMap.remove(session.getAccountId());
            }
        }
    }

    @Override
    public UserSession login(String email, String password) throws LoginException {
        if (email == null || email.isEmpty()) {
            throw new IllegalArgumentException("user cannot be null or empty");
        }
        if (password == null || password.isEmpty()) {
            throw new IllegalArgumentException("password cannot be null or empty");
        }
        Account userAccount = accountStorage.getAccount(email);
        if (userAccount == null) {
            throw new LoginException();
        }

        byte[] passwordHash = PasswordUtil.getPasswordHash(password, userAccount.getSalt());
        if (!Arrays.equals(userAccount.getHashedPassword(), passwordHash)) {
            throw new LoginException();
        }

        return login(userAccount);
    }

    @Override
    public UserSession login(Account userAccount) {
        UserSession session = getSession(userAccount);

        if (session == null) {
            session = createSession(userAccount);
        }

        return session;
    }


    private UserSession createSession(Account userAccount) {
        List<GameCharacter> userCharacters = accountStorage.getCharacters(userAccount.getAccountId());
        UserSession session = new UserSession(userAccount.getEmail(), timeProvider.getCurrentTimeEpoch(), timeProvider.getCurrentTimeEpoch(), sessionTimeoutInterval, userCharacters);
        synchronized (sessionMap) {
            sessionMap.put(session.getSessionId(), session);
            accountIdSessionIdMap.put(userAccount.getAccountId(), session.getSessionId());
        }
        return null;
    }

    private UserSession getSession(Account userAccount) {
        synchronized (sessionMap) {
            String sessionId = accountIdSessionIdMap.get(userAccount.getAccountId());
            if(sessionId == null) {
                return null;
            }
            return sessionMap.get(sessionId);
        }
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    private boolean isValidSession(UserSession session) {
        return session != null && (session.getLastSessionRefresh() + sessionTimeoutInterval) > timeProvider.getCurrentTimeEpoch();
    }
}
