package com.bitcrunzh.portal.internal.services.exceptions;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import java.text.ParseException;

public class ParseExceptionMapper implements ExceptionMapper<ParseException> {
    @Override
    public Response toResponse(ParseException e) {
        return Response.status(Response.Status.BAD_REQUEST).entity("Failed to parse input").build();
    }
}
