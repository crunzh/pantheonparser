package com.bitcrunzh.portal.internal.parser.eq;

/**
 * Created by RedDevil on 26-12-2017.
 */
public enum ChatType {
    Channel,
    Say,
    Shout,
    Tell,
    Guild,
    Yell,
    Group,
    Unknown
}
