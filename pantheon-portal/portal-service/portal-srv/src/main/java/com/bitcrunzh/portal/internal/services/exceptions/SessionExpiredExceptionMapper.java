package com.bitcrunzh.portal.internal.services.exceptions;

import com.bitcrunzh.portal.api.model.user.SessionExpiredException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

public class SessionExpiredExceptionMapper implements ExceptionMapper<SessionExpiredException> {
    @Override
    public Response toResponse(SessionExpiredException e) {
        return Response.status(Response.Status.UNAUTHORIZED).entity("Session Expired").build();
    }
}
