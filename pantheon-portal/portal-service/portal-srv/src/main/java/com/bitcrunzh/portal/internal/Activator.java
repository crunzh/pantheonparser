package com.bitcrunzh.portal.internal;

import com.bitcrunzh.portal.api.LoginService;
import com.bitcrunzh.portal.api.UploadService;
import com.bitcrunzh.portal.internal.account.SessionCache;
import com.bitcrunzh.portal.internal.account.SessionCacheImpl;
import com.bitcrunzh.portal.internal.services.LoginServiceImpl;
import com.bitcrunzh.portal.internal.services.UploadServiceImpl;
import com.bitcrunzh.portal.internal.storage.FileStorageImpl;
import com.bitcrunzh.portal.internal.storage.MongoModelStorageImpl;
import com.bitcrunzh.portal.internal.storage.ServerConfiguration;
import com.bitcrunzh.portal.internal.storage.ServerConfigurationImpl;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

import java.util.Dictionary;
import java.util.Hashtable;

public class Activator implements BundleActivator {
    private MongoModelStorageImpl modelStorage;
    private FileStorageImpl fileStorage;
    private SessionCache sessionCache;


    @Override
    public void start(BundleContext bundleContext) throws Exception {
        TimeProvider timeProvider = new TimeProviderImpl();
        ServerConfiguration serverConfiguration = new ServerConfigurationImpl();
        modelStorage = new MongoModelStorageImpl(serverConfiguration.getMongoServers());
        fileStorage = new FileStorageImpl(serverConfiguration.getFileDirectory(), serverConfiguration.getMaxFileSize());
        sessionCache = new SessionCacheImpl(modelStorage.getAccountStorage(), timeProvider, serverConfiguration.getSessionTimeoutInterval());
        EventHandlerFactoryImpl eventHandlerFactory = new EventHandlerFactoryImpl(modelStorage);

        LoginService loginService = new LoginServiceImpl(modelStorage.getAccountStorage(), sessionCache);
        UploadServiceImpl uploadService = new UploadServiceImpl(eventHandlerFactory, sessionCache, modelStorage, fileStorage);

        registerRestService(bundleContext, LoginService.class, loginService);
        registerRestService(bundleContext, UploadService.class, uploadService);
    }



    @Override
    public void stop(BundleContext bundleContext) throws Exception {

    }

    private static <T> ServiceRegistration<T> registerRestService(BundleContext bundleContext, Class<T> serviceClass, T implementation) {
        Dictionary<String, Object> serviceConfiguration = new Hashtable<>();
        return bundleContext.registerService(serviceClass, implementation, serviceConfiguration);
    }
}
