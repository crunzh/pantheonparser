package com.bitcrunzh.portal.internal.storage;

import com.mongodb.ServerAddress;

import java.io.File;

public interface ServerConfiguration {
    int getSessionTimeoutInterval();

    File getFileDirectory();

    int getMaxFileSize();

    ServerAddress getMongoServers();
}
