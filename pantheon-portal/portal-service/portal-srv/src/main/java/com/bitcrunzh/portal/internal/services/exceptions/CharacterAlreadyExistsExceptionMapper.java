package com.bitcrunzh.portal.internal.services.exceptions;

import com.bitcrunzh.portal.api.model.user.CharacterAlreadyExistsException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

public class CharacterAlreadyExistsExceptionMapper implements ExceptionMapper<CharacterAlreadyExistsException> {
    @Override
    public Response toResponse(CharacterAlreadyExistsException e) {
        return Response.status(Response.Status.CONFLICT).entity("Character already exists").build();
    }
}
