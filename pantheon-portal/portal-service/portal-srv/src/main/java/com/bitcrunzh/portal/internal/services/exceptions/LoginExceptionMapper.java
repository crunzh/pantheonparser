package com.bitcrunzh.portal.internal.services.exceptions;

import com.bitcrunzh.portal.api.model.user.LoginException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

public class LoginExceptionMapper implements ExceptionMapper<LoginException> {
    @Override
    public Response toResponse(LoginException e) {
        return Response.status(Response.Status.UNAUTHORIZED).entity("Failed to login").build();
    }
}
