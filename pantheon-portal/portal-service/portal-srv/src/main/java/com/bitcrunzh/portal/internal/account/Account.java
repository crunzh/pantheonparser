package com.bitcrunzh.portal.internal.account;

import java.util.UUID;

public class Account {
    private UUID accountId;
    private String email;
    private byte[] hashedPassword;
    private byte[] salt;

    public Account() {
    }

    public Account(String email, byte[] hashedPassword, byte[] salt) {
        this(UUID.randomUUID(), email, hashedPassword, salt);
    }

    public Account(UUID accountId, String email, byte[] hashedPassword, byte[] salt) {
        this.accountId = accountId;
        this.email = email;
        this.hashedPassword = hashedPassword;
        this.salt = salt;
    }

    public UUID getAccountId() {
        return accountId;
    }

    public void setAccountId(UUID accountId) {
        this.accountId = accountId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public byte[] getHashedPassword() {
        return hashedPassword;
    }

    public void setHashedPassword(byte[] hashedPassword) {
        this.hashedPassword = hashedPassword;
    }

    public byte[] getSalt() {
        return salt;
    }

    public void setSalt(byte[] salt) {
        this.salt = salt;
    }
}
