package com.bitcrunzh.portal.internal.parser.eq.parsers;

import com.bitcrunzh.portal.api.model.GameCharacter;
import com.bitcrunzh.portal.api.model.log.chat.Chat;
import com.bitcrunzh.portal.api.model.log.chat.Tell;
import com.bitcrunzh.portal.internal.parser.LogLineParserBase;
import com.bitcrunzh.portal.internal.parser.ParserVisitor;
import com.bitcrunzh.portal.internal.parser.eq.ChatType;
import com.bitcrunzh.portal.internal.parser.eq.ParserUtil;
import com.bitcrunzh.portal.internal.storage.ModelStorage;

import java.text.ParseException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by RedDevil on 26-12-2017.
 */
public class ChatSentParser extends LogLineParserBase {


    protected ChatSentParser() {
        super(Pattern.compile("\\[(.+)]\\sYou\\s(say|says|tell|tells|told|shout|shouts|yell|yells)(.*)\\s(\\S+)?,\\s'(.+)'"));
    }

    @Override
    public void consume(Matcher matcher, GameCharacter character, ParserVisitor visitor) throws ParseException {
        /*
        1) Time
        2) Tell/Say etc.
        3) words between tell and chatDst
        4) Chat Dst
        5) Chat Message
         */
        long time = ParserUtil.parseLogTime(matcher.group(1));
        ChatType chatType = resolveChatType(matcher);
        String channelName = null;
        String chatDst = matcher.group(4);
        String message = matcher.group(5);

        switch (chatType) {
            case Tell:
                visitor.visit(new Tell(character.getCharacterName(), character.getCharacterId(), chatDst, null, message, time));
                break;
            default:
                visitor.visit(new Chat(character.getCharacterName(), character.getCharacterId(), message, chatType.name(), channelName, time));

        }
    }

    private ChatType resolveChatType(Matcher matcher) {
        switch (matcher.group(2).toLowerCase()) {
            case "tell":
            case "told":
            case "tells":
                return ChatType.Tell;
            case "say":
            case "says":
                return ChatType.Say;
            case "shout":
            case "shouts":
                return ChatType.Shout;
            case "yell":
            case "yells":
                return ChatType.Yell;
            case "guild":
                return ChatType.Guild;
            case "group":
                return ChatType.Group;
            case "channel":
                return ChatType.Channel;
        }
        return ChatType.Unknown;
    }
}
