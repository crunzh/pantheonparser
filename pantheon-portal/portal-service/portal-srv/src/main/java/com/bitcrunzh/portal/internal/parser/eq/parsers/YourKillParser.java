package com.bitcrunzh.portal.internal.parser.eq.parsers;

import com.bitcrunzh.portal.api.model.GameCharacter;
import com.bitcrunzh.portal.api.model.log.combat.Kill;
import com.bitcrunzh.portal.internal.parser.LogLineParserBase;
import com.bitcrunzh.portal.internal.parser.ParserVisitor;
import com.bitcrunzh.portal.internal.parser.eq.ParserUtil;

import java.text.ParseException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by RedDevil on 10-05-2017.
 */
public class YourKillParser extends LogLineParserBase {
    public YourKillParser() {
        super(Pattern.compile("\\[(.*)]\\sYou have slain\\s(.+)!"));
    }

    @Override
    public void consume(Matcher matcher, GameCharacter character, ParserVisitor visitor) throws ParseException {
        visitor.visit(new Kill(
                character.getCharacterName(),
                character.getCharacterId(),
                matcher.group(2),
                null,
                null,
                ParserUtil.parseLogTime(matcher.group(1))
        ));
    }
}
