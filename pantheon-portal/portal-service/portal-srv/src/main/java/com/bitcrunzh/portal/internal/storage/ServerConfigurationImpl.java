package com.bitcrunzh.portal.internal.storage;

import com.mongodb.ServerAddress;

import java.io.File;

public class ServerConfigurationImpl implements ServerConfiguration {
    @Override
    public int getSessionTimeoutInterval() {
        return 0;
    }

    @Override
    public File getFileDirectory() {
        return null;
    }

    @Override
    public int getMaxFileSize() {
        return 0;
    }

    @Override
    public ServerAddress getMongoServers() {
        return null;
    }
}
