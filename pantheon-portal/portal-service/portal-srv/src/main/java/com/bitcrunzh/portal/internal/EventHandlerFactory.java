package com.bitcrunzh.portal.internal;

import com.bitcrunzh.portal.api.model.GameCharacter;
import com.bitcrunzh.portal.internal.account.UserSession;
import com.bitcrunzh.portal.internal.parser.ParserVisitor;

public interface EventHandlerFactory {
    ParserVisitor create(UserSession session, GameCharacter characterId);
}
