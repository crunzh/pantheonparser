package com.bitcrunzh.portal.internal.storage;

import com.bitcrunzh.portal.api.model.upload.Screenshot;

import java.util.List;
import java.util.UUID;

public interface CharacterStorage {
    List<Screenshot> getAllScreenshots(UUID characterId);
    long getSumOfScreenshotSize(UUID characterId);
    void addScreenshot(Screenshot screenshot);
}
