package com.bitcrunzh.portal.internal.parser.eq.parsers;

import com.bitcrunzh.portal.api.model.GameCharacter;
import com.bitcrunzh.portal.api.model.log.loot.ItemLoot;
import com.bitcrunzh.portal.internal.parser.LogLineParserBase;
import com.bitcrunzh.portal.internal.parser.ParserVisitor;
import com.bitcrunzh.portal.internal.parser.eq.ParserUtil;

import java.text.ParseException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by RedDevil on 10-05-2017.
 */
public class ItemLootParser extends LogLineParserBase {
    public ItemLootParser() {
        super(Pattern.compile("\\[(.*)]\\s--You have looted a\\s(.+)\\.--"));
    }

    @Override
    public void consume(Matcher matcher, GameCharacter character, ParserVisitor visitor) throws ParseException {
        //TODO lookup item id
        visitor.visit(new ItemLoot(
                character.getCharacterName(),
                character.getCharacterId(),
                matcher.group(2),
                null,
                ParserUtil.parseLogTime(matcher.group(1))
        ));
    }
}
