package com.bitcrunzh.portal.internal.parser.eq.parsers;

import com.bitcrunzh.portal.api.model.GameCharacter;
import com.bitcrunzh.portal.api.model.log.combat.MeleeDamage;
import com.bitcrunzh.portal.internal.parser.LogLineParserBase;
import com.bitcrunzh.portal.internal.parser.ParserVisitor;
import com.bitcrunzh.portal.internal.parser.eq.ParserUtil;

import java.text.ParseException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class YourDamageTakenParser extends LogLineParserBase {
    public YourDamageTakenParser() {
        super(Pattern.compile("\\[(.*)]\\s(.+)\\s(\\w+)\\sYOU for\\s(\\d+)\\spoints of damage\\."));
    }

    @Override
    public void consume(Matcher matcher, GameCharacter character, ParserVisitor visitor) throws ParseException {
        visitor.visit(new MeleeDamage(
                ParserUtil.parseLogTime(matcher.group(1)),
                matcher.group(2),
                null,
                null,
                character.getCharacterName(),
                character.getCharacterId(),
                null,
                matcher.group(3),
                Integer.parseInt(matcher.group(4)),
                null
        ));
    }
}
