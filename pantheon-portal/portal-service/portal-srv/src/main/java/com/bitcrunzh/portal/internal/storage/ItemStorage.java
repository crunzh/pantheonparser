package com.bitcrunzh.portal.internal.storage;

import com.bitcrunzh.portal.api.model.log.equipment.Item;

import java.util.UUID;

public interface ItemStorage {
    Item getItem(UUID itemId);
    Item getItem(String itemName, String gameType);
    void setItem(Item item);
}
