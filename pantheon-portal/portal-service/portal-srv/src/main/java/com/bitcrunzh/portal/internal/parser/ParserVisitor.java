package com.bitcrunzh.portal.internal.parser;

import com.bitcrunzh.portal.api.model.log.advancement.LevelUp;
import com.bitcrunzh.portal.api.model.log.advancement.SkillAdvancement;
import com.bitcrunzh.portal.api.model.log.chat.Chat;
import com.bitcrunzh.portal.api.model.log.chat.Tell;
import com.bitcrunzh.portal.api.model.log.equipment.ItemEquipped;
import com.bitcrunzh.portal.api.model.log.combat.CharacterDeath;
import com.bitcrunzh.portal.api.model.log.combat.Kill;
import com.bitcrunzh.portal.api.model.log.loot.ItemLoot;
import com.bitcrunzh.portal.api.model.log.loot.MoneyLoot;
import com.bitcrunzh.portal.api.model.log.magic.*;
import com.bitcrunzh.portal.api.model.log.organisation.*;
import com.bitcrunzh.portal.api.model.log.position.LogIn;
import com.bitcrunzh.portal.api.model.log.position.LogOut;
import com.bitcrunzh.portal.api.model.log.position.PositionReport;
import com.bitcrunzh.portal.api.model.log.position.ZoneChange;
import com.bitcrunzh.portal.api.model.log.combat.MeleeDamage;

public interface ParserVisitor {
    default void visit(LevelUp obj) {}
    default void visit(SkillAdvancement obj) {}
    default void visit(Chat obj) {}
    default void visit(Tell obj) {}
    default void visit(CharacterDeath obj) {}
    default void visit(Kill obj) {}
    default void visit(MeleeDamage obj) {}
    default void visit(Item obj) {}
    default void visit(ItemEquipped obj) {}
    default void visit(ItemLoot obj) {}
    default void visit(MoneyLoot obj) {}
    default void visit(CastSpell obj) {}
    default void visit(Dispell obj) {}
    default void visit(Fizzle obj) {}
    default void visit(PetSummon obj) {}
    default void visit(SpellEffectElapsed obj) {}
    default void visit(JoinGroup obj) {}
    default void visit(JoinGuild obj) {}
    default void visit(JoinRaid obj) {}
    default void visit(LeaveGroup obj) {}
    default void visit(LeaveGuild obj) {}
    default void visit(LeaveRaid obj) {}
    default void visit(LogIn obj) {}
    default void visit(LogOut obj) {}
    default void visit(PositionReport obj) {}
    default void visit(ZoneChange obj) {}
}
