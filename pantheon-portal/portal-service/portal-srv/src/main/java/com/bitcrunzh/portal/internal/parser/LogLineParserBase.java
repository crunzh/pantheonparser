package com.bitcrunzh.portal.internal.parser;

import java.util.regex.Pattern;

public abstract class LogLineParserBase implements LogLineParser {
    private final Pattern pattern;
    private final boolean isMultiSentenceLine;

    protected LogLineParserBase(Pattern pattern, boolean isMultiSentenceLine) {
        this.pattern = pattern;
        this.isMultiSentenceLine = isMultiSentenceLine;
    }

    public LogLineParserBase(Pattern pattern) {
        this(pattern, false);
    }

    @Override
    public boolean isMultiSentenceLine() {
        return isMultiSentenceLine;
    }

    @Override
    public Pattern getPattern() {
        return pattern;
    }
}
