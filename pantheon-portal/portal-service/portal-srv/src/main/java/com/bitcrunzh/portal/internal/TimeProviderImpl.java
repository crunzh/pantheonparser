package com.bitcrunzh.portal.internal;

public class TimeProviderImpl implements TimeProvider {
    @Override
    public long getCurrentTimeEpoch() {
        return System.currentTimeMillis();
    }
}
