package com.bitcrunzh.portal.internal.parser;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by RedDevil on 29-04-2017.
 */
public class ParserUtil {
    private static final SimpleDateFormat DATE_TIME_FORMATTER = new SimpleDateFormat("EEE MMM d HH:mm:ss YYYY", Locale.ENGLISH);
    public static long parseLogTime(String dateTimeString) throws ParseException {
        Date date = DATE_TIME_FORMATTER.parse(dateTimeString);
        return date.getTime();
    }
}
