package com.bitcrunzh.portal.internal.parser.eq.parsers;

import com.bitcrunzh.portal.api.model.GameCharacter;
import com.bitcrunzh.portal.api.model.log.advancement.LevelUp;
import com.bitcrunzh.portal.internal.parser.LogLineParserBase;
import com.bitcrunzh.portal.internal.parser.ParserVisitor;
import com.bitcrunzh.portal.internal.parser.eq.ParserUtil;
import com.bitcrunzh.portal.internal.storage.ModelStorage;

import java.text.ParseException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by RedDevil on 29-04-2017.
 */
public class LevelUpParser extends LogLineParserBase {

    public LevelUpParser() {
        super(Pattern.compile("\\[(.*)]\\sYou have gained a level! Welcome to level\\s(\\d+)!"));
    }

    @Override
    public void consume(Matcher matcher, GameCharacter character, ParserVisitor visitor) throws ParseException {
        visitor.visit(new LevelUp(
                character.getCharacterName(),
                character.getCharacterId(),
                Integer.parseInt(matcher.group(2)),
                ParserUtil.parseLogTime(matcher.group(1))
        ));
    }
}
