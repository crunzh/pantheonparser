package com.bitcrunzh.portal.internal;

import com.bitcrunzh.portal.api.model.GameCharacter;
import com.bitcrunzh.portal.api.model.log.equipment.Item;
import com.bitcrunzh.portal.api.model.log.loot.ItemLoot;
import com.bitcrunzh.portal.api.model.log.position.LogIn;
import com.bitcrunzh.portal.internal.account.UserSession;
import com.bitcrunzh.portal.internal.parser.ParserVisitor;
import com.bitcrunzh.portal.internal.storage.ModelStorage;

import java.util.UUID;

public class EventHandlerFactoryImpl implements EventHandlerFactory {
    private final ModelStorage modelStorage;

    EventHandlerFactoryImpl(ModelStorage modelStorage) {
        this.modelStorage = modelStorage;
    }

    @Override
    public ParserVisitor create(UserSession session, GameCharacter character) {
        return new ParserVisitor() {
            @Override
            public void visit(LogIn obj) {

            }

            @Override
            public void visit(ItemLoot obj) {
                Item item = modelStorage.getItemStorage().getItem(obj.getItemName(), character.getGameType());
                if(item == null) {
                    item = createItem(obj, character.getGameType());
                }
                obj.setItemId(item.getItemId());
            }
        };
    }

    private Item createItem(ItemLoot obj, String gameType) {
        Item item = new Item(UUID.randomUUID(), obj.getItemName(), gameType, null, null);
        modelStorage.getItemStorage().setItem(item);
        return item;
    }
}
