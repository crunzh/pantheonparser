package com.bitcrunzh.portal.internal.account;

import com.bitcrunzh.portal.api.model.GameCharacter;

import java.util.List;
import java.util.UUID;

public class UserSession {
    private String sessionId;
    private UUID accountId;
    private String email;
    private long sessionStartTime;
    private volatile long lastSessionRefresh;
    private int sessionTimeoutInterval;
    private List<GameCharacter> userCharacters;

    public UserSession() {
    }

    public UserSession(String userId, long sessionStartTime, long lastSessionRefresh, int sessionTimeoutInterval, List<GameCharacter> userCharacters) {
        this(UUID.randomUUID().toString(), userId, sessionStartTime, lastSessionRefresh, sessionTimeoutInterval, userCharacters);
    }

    public UserSession(String sessionId, String email, long sessionStartTime, long lastSessionRefresh, int sessionTimeoutInterval, List<GameCharacter> userCharacters) {
        this.sessionId = sessionId;
        this.email = email;
        this.sessionStartTime = sessionStartTime;
        this.lastSessionRefresh = lastSessionRefresh;
        this.sessionTimeoutInterval = sessionTimeoutInterval;
        this.userCharacters = userCharacters;
    }

    public UUID getAccountId() {
        return accountId;
    }

    public void setAccountId(UUID accountId) {
        this.accountId = accountId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String userId) {
        this.email = userId;
    }

    public long getSessionStartTime() {
        return sessionStartTime;
    }

    public void setSessionStartTime(long sessionStartTime) {
        this.sessionStartTime = sessionStartTime;
    }

    public long getLastSessionRefresh() {
        return lastSessionRefresh;
    }

    public void setLastSessionRefresh(long lastSessionRefresh) {
        this.lastSessionRefresh = lastSessionRefresh;
    }

    public int getSessionTimeoutInterval() {
        return sessionTimeoutInterval;
    }

    public void setSessionTimeoutInterval(int sessionTimeoutInterval) {
        this.sessionTimeoutInterval = sessionTimeoutInterval;
    }

    public List<GameCharacter> getAccountCharacters() {
        return userCharacters;
    }

    public void setUserCharacters(List<GameCharacter> userCharacters) {
        this.userCharacters = userCharacters;
    }
}
