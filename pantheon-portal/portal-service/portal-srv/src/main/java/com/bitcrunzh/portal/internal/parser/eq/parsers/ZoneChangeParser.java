package com.bitcrunzh.portal.internal.parser.eq.parsers;

import com.bitcrunzh.portal.api.model.GameCharacter;
import com.bitcrunzh.portal.api.model.log.position.ZoneChange;
import com.bitcrunzh.portal.internal.parser.LogLineParserBase;
import com.bitcrunzh.portal.internal.parser.ParserVisitor;
import com.bitcrunzh.portal.internal.parser.eq.ParserUtil;

import java.text.ParseException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by RedDevil on 10-05-2017.
 */
public class ZoneChangeParser extends LogLineParserBase {
    public ZoneChangeParser() {
        super(Pattern.compile("\\[(.*)]\\sYou have entered\\s(.+)\\."));
    }

    @Override
    public void consume(Matcher matcher, GameCharacter character, ParserVisitor visitor) throws ParseException {
        visitor.visit(new ZoneChange(
                character.getCharacterName(),
                character.getCharacterId(),
                ParserUtil.parseLogTime(matcher.group(1)),
                matcher.group(2)
        ));
    }
}
