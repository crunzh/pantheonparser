package com.bitcrunzh.portal.internal.storage.model;

import java.util.UUID;

public class FileSaveResult {
    private UUID fileID;
    private String fileType;
    private int fileSize;

    public FileSaveResult() {
    }

    public FileSaveResult(UUID fileID, String fileType, int fileSize) {
        this.fileID = fileID;
        this.fileType = fileType;
        this.fileSize = fileSize;
    }

    public UUID getFileID() {
        return fileID;
    }

    public void setFileID(UUID fileID) {
        this.fileID = fileID;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public int getFileSize() {
        return fileSize;
    }

    public void setFileSize(int fileSize) {
        this.fileSize = fileSize;
    }
}
