package com.bitcrunzh.portal.internal.storage;

import com.bitcrunzh.portal.api.model.GameCharacter;
import com.bitcrunzh.portal.internal.account.Account;

import java.util.List;
import java.util.UUID;

public interface AccountStorage {
    Account getAccount(String email);

    void setAccount(Account account);

    void deleteAccount(UUID accountId);

    List<GameCharacter> getCharacters(UUID accountId);

    void setCharacter(GameCharacter character);

    void deleteCharacter(UUID characterID);
}
