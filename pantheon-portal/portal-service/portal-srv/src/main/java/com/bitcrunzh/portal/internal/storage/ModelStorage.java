package com.bitcrunzh.portal.internal.storage;

public interface ModelStorage {
    AccountStorage getAccountStorage();
    ItemStorage getItemStorage();

    CharacterStorage getCharacterStorage();
}
