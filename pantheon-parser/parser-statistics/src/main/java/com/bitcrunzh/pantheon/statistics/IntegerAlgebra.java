package com.bitcrunzh.pantheon.statistics;

public class IntegerAlgebra implements Algebra<Integer> {
    @Override
    public Integer add(Integer first, Integer second) {
        first = unBox(first);
        second = unBox(second);
        return first + second;
    }

    @Override
    public Integer subtract(Integer toSubtractFrom, Integer toSubtract) {
        toSubtractFrom = unBox(toSubtractFrom);
        toSubtract = unBox(toSubtract);
        return toSubtractFrom - toSubtract;
    }

    @Override
    public Integer getZero() {
        return 0;
    }

    private int unBox(Integer integer) {
        if(integer == null) {
            return 0;
        }
        return integer;
    }
}
