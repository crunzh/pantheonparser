package com.bitcrunzh.pantheon.statistics;

public class DefaultTimeProvider implements TimeProvider {
    @Override
    public long getTime() {
        return System.currentTimeMillis();
    }
}
