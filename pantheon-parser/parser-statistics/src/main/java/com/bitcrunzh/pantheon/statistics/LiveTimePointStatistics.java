package com.bitcrunzh.pantheon.statistics;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class LiveTimePointStatistics<T> {
    private final Queue<MeasurementPoint<T>> measurements = new LinkedList<>(); //TODO sort to keep consistency no matter order of addition?
    private final Algebra<T> algebra;
    private final TimeProvider timeProvider;
    private T sum;

    LiveTimePointStatistics(Algebra<T> algebra, TimeProvider timeProvider) {
        this.algebra = algebra;
        sum = algebra.getZero();
        this.timeProvider = timeProvider;
    }

    public synchronized void add(MeasurementPoint<T> point) {
        measurements.add(point);
        sum = algebra.add(sum, point.getMeasurement());
    }

    public synchronized List<MeasurementPoint<T>> cutTail(int timeOffsetMs) {
        List<MeasurementPoint<T>> cutTail = new ArrayList<>();
        long now = timeProvider.getTime();
        while (!measurements.isEmpty() && (measurements.peek().getMeasurementTime() + timeOffsetMs) < now) {
            MeasurementPoint<T> measurementPoint = measurements.remove();
            cutTail.add(measurementPoint);
            sum = algebra.subtract(sum, measurementPoint.getMeasurement());
        }
        return cutTail;
    }

    public T getSum() {
        return sum;
    }

    public synchronized void addAll(List<MeasurementPoint<T>> points) {
        for(MeasurementPoint<T> point : points) {
            add(point);
        }
    }
}
