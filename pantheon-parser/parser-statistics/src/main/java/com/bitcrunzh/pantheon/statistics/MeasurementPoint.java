package com.bitcrunzh.pantheon.statistics;

public class MeasurementPoint<T> {
    private final long measurementTime;
    private final T measurement;

    MeasurementPoint(long measurementTime, T measurement) {
        this.measurementTime = measurementTime;
        this.measurement = measurement;
    }

    public long getMeasurementTime() {
        return measurementTime;
    }

    public T getMeasurement() {
        return measurement;
    }
}
