package com.bitcrunzh.pantheon.statistics;

import java.util.ArrayList;
import java.util.List;

public class LiveTimeMultiPointStatistics<T> {
    private final List<LiveTimePointStatistics<T>> timePoints;
    private final int timeIntervalMs;
    private final TimeProvider timeProvider;

    public LiveTimeMultiPointStatistics(int numberOfPoints, int timeIntervalMs, Algebra<T> algebra, TimeProvider timeProvider) {
        timePoints = new ArrayList<>(numberOfPoints);
        this.timeIntervalMs = timeIntervalMs;
        this.timeProvider = timeProvider;
        for (int i = 0; i < numberOfPoints; i++) {
            timePoints.add(new LiveTimePointStatistics<>(algebra, timeProvider));
        }
    }

    public void addMeasurement(T measurement) {
        timePoints.get(0).add(new MeasurementPoint<>(timeProvider.getTime(), measurement));
    }

    public List<MeasurementPoint<T>> getMeasurementPoints() {
        List<MeasurementPoint<T>> points = new ArrayList<>();
        long time = timeProvider.getTime();
        List<MeasurementPoint<T>> oldPoints = new ArrayList<>();
        for (LiveTimePointStatistics<T> pointStatistics : timePoints) {
            pointStatistics.addAll(oldPoints);
            oldPoints = pointStatistics.cutTail(timeIntervalMs);
            points.add(new MeasurementPoint<>(time, pointStatistics.getSum()));
            time -= timeIntervalMs;
        }
        return points;
    }
}
