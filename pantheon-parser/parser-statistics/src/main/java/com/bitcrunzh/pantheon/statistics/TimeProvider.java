package com.bitcrunzh.pantheon.statistics;

public interface TimeProvider {
    long getTime();
}
