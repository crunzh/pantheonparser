package com.bitcrunzh.pantheon.statistics;

public interface Algebra<T> {
    T add(T first, T second);
    T subtract(T toSubtractFrom, T toSubtract);
    T getZero();
}
