package com.bitcrunzh.pantheon.gui;

import com.bitcrunzh.pantheon.parser.model.MultiEventTypeListener;
import com.bitcrunzh.pantheon.parser.model.events.*;

/**
 * Created by RedDevil on 29-04-2017.
 */
public class EventLogger {
    private final MultiEventTypeListener eventListener = new MultiEventTypeListener();

    public EventLogger() {
        eventListener.addListener(YouGainedALevel.class, event -> System.out.println("You gained a level: "+event.getLevel()+" at "+event.getLogTime()+" ms. after EPOCH."));
        eventListener.addListener(YourDamageGiven.class, event -> System.out.println(">> You "+event.getDamageType()+" for "+event.getDamageGiven()+" points of damage."));
        eventListener.addListener(YourDamageTaken.class, event -> System.out.println("<< You are "+event.getDamageType()+" for "+event.getDamageTaken()+" points of damage."));
        eventListener.addListener(SkillUp.class, event -> System.out.println("Skill Up! "+event.getSkillType()+ " "+event.getSkillLevel()));
        eventListener.addListener(YouHaveLooted.class, event -> System.out.println("Looted "+event.getItem()));
        eventListener.addListener(YouHaveSlain.class, event -> System.out.println("Killed "+event.getSlayedName()));
        eventListener.addListener(ZoneChange.class, event -> System.out.println("Entered zone "+event.getZoneName()));
        eventListener.addListener(Location.class, event -> System.out.println("Location is now x:"+event.getxCoordinate()+" y:"+event.getyCoordinate()+" z:"+event.getzCoordinate()));
    }

    public MultiEventTypeListener getEventListener() {
        return eventListener;
    }
}
