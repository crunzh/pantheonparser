package com.bitcrunzh.pantheon.parser.exception;

import com.bitcrunzh.portal.api.model.user.CharacterAlreadyExistsException;
import org.apache.cxf.jaxrs.client.ResponseExceptionMapper;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

public class CharacterAlreadyExistsExceptionMapper implements ResponseExceptionMapper<CharacterAlreadyExistsException> {
    @Override
    public CharacterAlreadyExistsException fromResponse(Response r) {
        return new CharacterAlreadyExistsException();
    }
}
