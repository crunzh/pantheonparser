package com.bitcrunzh.pantheon.parser;

import com.bitcrunzh.pantheon.parser.settings.ParserConfiguration;
import com.bitcrunzh.portal.api.LoginService;
import com.bitcrunzh.portal.api.UploadService;
import com.bitcrunzh.portal.api.model.user.LoginException;
import com.bitcrunzh.portal.api.model.user.Session;
import com.bitcrunzh.portal.api.model.user.SessionExpiredException;
import org.apache.cxf.jaxrs.client.JAXRSClientFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.WebApplicationException;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class PortalConnection {
    private static final Logger LOGGER = LoggerFactory.getLogger(PortalConnection.class);
    private final ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(1);
    private final ParserConfiguration parserConfiguration;
    private final List<PortalConnectionChangeListener> listeners = new CopyOnWriteArrayList<>();
    private Session session = null;

    public PortalConnection(ParserConfiguration parserConfiguration) {
        this.parserConfiguration = parserConfiguration;
    }

    void start() {
        scheduledExecutorService.scheduleWithFixedDelay(() -> tryEstablishSession(), 0, parserConfiguration.getPortalSessionCheckIntervalSeconds(), TimeUnit.SECONDS);
    }

    private UploadService getUploadService() {
        UploadService uploadService = JAXRSClientFactory.create(parserConfiguration.getPortalAddress(), UploadService.class);
        return new UploadServiceProxy(uploadService);
    }

    private LoginService getLoginService() {
        LoginService loginService = JAXRSClientFactory.create(parserConfiguration.getPortalAddress(), LoginService.class);
        return null;
    }

    void dispose() {
        scheduledExecutorService.shutdownNow();
        //TODO dispose client proxies.
    }

    void addConnectionListener(PortalConnectionChangeListener listener) {
        listeners.add(listener);
    }

    public interface PortalConnectionChangeListener {
        void connectionClosed();

        void sessionEstablished(Session session, LoginService loginService, UploadService uploadService);
    }

    private void notifyConnectionClosed() {
        for (PortalConnectionChangeListener listener : listeners) {
            listener.connectionClosed();
        }
    }

    private void notifySessionEstablished(Session session, LoginService loginService, UploadService uploadService) {
        for (PortalConnectionChangeListener listener : listeners) {
            listener.sessionEstablished(session, loginService, uploadService);
        }
    }

    private void tryEstablishSession() {
        if (session != null && !session.isSessionTimedOut(System.currentTimeMillis())) {
            return;
        }
        try {
            session = getLoginService().login(parserConfiguration.getAccountEmail(), parserConfiguration.getPassword());
            notifySessionEstablished(session, getLoginService(), getUploadService());
        } catch (LoginException e) {
            e.printStackTrace();
        }
    }

    private static class UploadServiceProxy implements UploadService {
        private final UploadService uploadService;

        private UploadServiceProxy(UploadService uploadService) {
            this.uploadService = uploadService;
        }

        @Override
        public void uploadLogFile(String sessionId, String characterId, InputStream logFile) throws ParseException, SessionExpiredException, IOException {
            try {
                uploadService.uploadLogFile(sessionId, characterId, logFile);
            } catch (WebApplicationException e) {
                //TODO catch connectivity exceptions
            }
        }

        @Override
        public void uploadLogLine(String sessionId, String characterId, String logLine) throws ParseException, SessionExpiredException {

        }

        @Override
        public void uploadImage(String sessionId, String characterId, long lastModified, InputStream image) throws SessionExpiredException {

        }
    }
}
