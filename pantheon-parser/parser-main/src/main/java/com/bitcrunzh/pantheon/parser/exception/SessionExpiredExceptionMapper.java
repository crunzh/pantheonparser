package com.bitcrunzh.pantheon.parser.exception;

import com.bitcrunzh.portal.api.model.user.SessionExpiredException;
import org.apache.cxf.jaxrs.client.ResponseExceptionMapper;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

public class SessionExpiredExceptionMapper implements ResponseExceptionMapper<SessionExpiredException> {

    @Override
    public SessionExpiredException fromResponse(Response r) {
        return new SessionExpiredException();
    }
}
