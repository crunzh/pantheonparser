package com.bitcrunzh.pantheon.parser.settings;

import com.bitcrunzh.portal.api.model.GameType;
import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.FileBasedConfiguration;
import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.builder.FileBasedConfigurationBuilder;
import org.apache.commons.configuration2.builder.fluent.Parameters;
import org.apache.commons.configuration2.ex.ConfigurationException;

import java.io.File;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;


/**
 * Make parser Configuration available.
 */
public class ParserConfiguration {
    private static final String CONFIG_FILE = "/etc/config.properties";

    private final Configuration configuration;

    private static final String LOG_FILE_DIRECTORY_KEY = "log.file.directory";
    private String logFileDirectoryValue = null;

    private static final String ACCOUNT_EMAIL_KEY = "user.email";
    private String accountEmailValue = null;

    private static final String ACCOUNT_PASSWORD_KEY = "user.password";
    private String accountPasswordValue = null;

    private static final String SCREEN_SHOT_DIR_KEY = "screen.shot.directory";
    private String screenShotDirectory = null;

    private static final String GAME_DIR_KEY = "game.directory";
    private String gameDirectory = null;

    private static final String PORTAL_SESSION_CHECK_INTERVAL_KEY = "portal.session.check.interval.seconds";
    private Long portalSessionCheckIntervalSeconds;
    private static final long DEF_SESSION_CHECK_INTERVAL_SEC = 30;

    private static final String PORTAL_ADDRESS_KEY = "portal.address";
    private String portalAddress;

    public ParserConfiguration(Path installationDirectory) throws ConfigurationException, URISyntaxException {
        Parameters params = new Parameters();
// Read data from this file
        File propertiesFile = Paths.get(installationDirectory.toString(), CONFIG_FILE).toFile();

        FileBasedConfigurationBuilder<FileBasedConfiguration> builder =
                new FileBasedConfigurationBuilder<FileBasedConfiguration>(PropertiesConfiguration.class)
                        .configure(params.fileBased().setFile(propertiesFile));
        this.configuration = builder.getConfiguration();
    }

    public String getLogFileDirectory() {
        if (logFileDirectoryValue == null) {
            logFileDirectoryValue = configuration.getString(LOG_FILE_DIRECTORY_KEY);
        }
        return logFileDirectoryValue;
    }

    public String getScreenshotDirectory() {
        if(screenShotDirectory == null) {
            screenShotDirectory = configuration.getString(SCREEN_SHOT_DIR_KEY);
        }
        return screenShotDirectory;
    }

    public String getGameDirectory() {
        if(gameDirectory == null) {
            gameDirectory = configuration.getString(GAME_DIR_KEY);
        }
        return gameDirectory;
    }

    public String getAccountEmail() {
        if (accountEmailValue == null) {
            accountEmailValue = configuration.getString(ACCOUNT_EMAIL_KEY);
        }
        return accountEmailValue;
    }

    public String getPassword() {
        if (accountPasswordValue == null) {
            accountPasswordValue = configuration.getString(ACCOUNT_PASSWORD_KEY);
        }
        return accountPasswordValue;
    }

    public long getPortalSessionCheckIntervalSeconds() {
        if (portalSessionCheckIntervalSeconds == null) {
            portalSessionCheckIntervalSeconds = configuration.getLong(PORTAL_SESSION_CHECK_INTERVAL_KEY, DEF_SESSION_CHECK_INTERVAL_SEC);
        }
        return portalSessionCheckIntervalSeconds;
    }

    public String getPortalAddress() {
        if(portalAddress == null) {
            portalAddress = configuration.getString(PORTAL_ADDRESS_KEY);
        }
        return portalAddress;
    }
}
