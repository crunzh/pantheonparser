package com.bitcrunzh.pantheon.parser;

/**
 * Created by RedDevil on 29-04-2017.
 */
public interface ExceptionCallback {
    void ExceptionOccurred(String message, Throwable e);
}
