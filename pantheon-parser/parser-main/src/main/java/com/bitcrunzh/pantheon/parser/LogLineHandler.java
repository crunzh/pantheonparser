package com.bitcrunzh.pantheon.parser;

import com.bitcrunzh.portal.api.LoginService;
import com.bitcrunzh.portal.api.UploadService;
import com.bitcrunzh.portal.api.model.GameCharacter;
import com.bitcrunzh.portal.api.model.GameType;
import com.bitcrunzh.portal.api.model.user.CharacterAlreadyExistsException;
import com.bitcrunzh.portal.api.model.user.Session;
import com.bitcrunzh.portal.api.model.user.SessionExpiredException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.text.ParseException;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created by RedDevil on 29-04-2017.
 */
class LogLineHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(LogLineHandler.class);
    private final File logFilePath;
    private final ExceptionCallback callback;
    private final UploadService uploadService;
    private final LoginService loginService;
    private final GameType gameType;
    private final String characterName;
    private final String serverName;
    private final Session session;
    private final CharacterActivityListener activityListener;
    private final ExecutorService executorService;
    private Future logListenerThread;
    private LogFileListener logFileListener;
    private UUID characterId;

    LogLineHandler(File logFilePath, ExceptionCallback callback, UploadService uploadService, LoginService loginService, GameType gameType, String characterName, String serverName, Session session, CharacterActivityListener activityListener) {
        this(logFilePath, callback, uploadService, loginService, gameType, characterName, serverName, session, activityListener, Executors.newSingleThreadExecutor());
    }

    LogLineHandler(File logFilePath, ExceptionCallback callback, UploadService uploadService, LoginService loginService, GameType gameType, String characterName, String serverName, Session session, CharacterActivityListener activityListener, ExecutorService executorService) {
        this.logFilePath = logFilePath;
        this.callback = callback;
        this.uploadService = uploadService;
        this.loginService = loginService;
        this.gameType = gameType;
        this.characterName = characterName;
        this.serverName = serverName;
        this.session = session;
        this.activityListener = activityListener;
        this.executorService = executorService;
    }

    void start() {
        characterId = getOrCreateCharacterId();
        logFileListener = new LogFileListener(logFilePath, (message, e) -> callback.ExceptionOccurred("Error in LogFileListener", e), false, line -> {
            try {
                uploadService.uploadLogLine(session.getSessionId(), characterId.toString(), line);
                activityListener.characterLogChanged(characterId);
            } catch (ParseException e) {
                LOGGER.warn("Portal failed to understand the line '" + line + "'", e);
            } catch (SessionExpiredException e) {
                LOGGER.error("Failed to send log line as session expired.");
            }
        });
        logListenerThread = executorService.submit(() -> logFileListener.start());
    }

    void dispose() {
        logFileListener.dispose();
        logListenerThread.cancel(false);
        executorService.shutdown();
    }

    private UUID getOrCreateCharacterId() {
        List<GameCharacter> gameCharacters;
        try {
            gameCharacters = loginService.getCharacters(session.getSessionId());
        } catch (SessionExpiredException e) {
            LOGGER.error("Failed to get characters for account as session expired.", e);
            return null;
        }
        for (GameCharacter character : gameCharacters) {
            if (character.getGameType() == gameType && character.getCharacterName().equalsIgnoreCase(characterName) && character.getServerName().equals(serverName)) {
                return character.getCharacterId();
            }
        }

        GameCharacter newCharacter;
        try {
            newCharacter = loginService.createCharacter(session.getSessionId(), gameType, characterName, serverName);
            return newCharacter.getCharacterId();
        } catch (SessionExpiredException e) {
            LOGGER.error("Failed to create character as session expired.", e);
        } catch (CharacterAlreadyExistsException e) {
            LOGGER.error("Failed to create character as character has already been created.", e);
        }
        return null;
    }

    interface CharacterActivityListener{
        void characterLogChanged(UUID characterId);
    }
}
