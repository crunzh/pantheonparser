package com.bitcrunzh.pantheon.parser;

import com.bitcrunzh.portal.api.UploadService;
import com.bitcrunzh.portal.api.model.user.Session;
import com.bitcrunzh.portal.api.model.user.SessionExpiredException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.*;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ScreenShotListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(LogLineHandler.class);
    private static final String MIME_TYPE_IMAGE = "image";
    private final WatchService watchService;
    private final Future fileWatchThread;
    private volatile boolean isDisposed = false;
    private volatile UUID activeCharacterId;

    ScreenShotListener(File screenshotDirectory, Session session, UploadService uploadService) throws IOException {
        this(screenshotDirectory, session, uploadService, Executors.newSingleThreadExecutor());
    }

    ScreenShotListener(File screenshotDirectory, Session session, UploadService uploadService, ExecutorService executorService) throws IOException {
        if(screenshotDirectory == null || !screenshotDirectory.isDirectory()) {
            throw new IllegalArgumentException("screenshotDirectory was not a directory.");
        }
        watchService = FileSystems.getDefault().newWatchService();

        Path directory = screenshotDirectory.toPath();
        directory.register(watchService, StandardWatchEventKinds.ENTRY_CREATE);

        fileWatchThread = executorService.submit(() -> watchForScreenShots(watchService, directory, session, uploadService));
    }

    public void setActiveCharacterId(UUID activeCharacterId) {
        this.activeCharacterId = activeCharacterId;
    }

    private void watchForScreenShots(WatchService watchService, Path directory, Session session, UploadService uploadService) {
        while (!isDisposed) {
            try {
                WatchKey watchKey = watchService.take();
                List<WatchEvent<?>> events = watchKey.pollEvents();

                for(WatchEvent<?> event : events) {
                    if(activeCharacterId == null) {
                        break;
                    }
                    if(event.kind() == StandardWatchEventKinds.OVERFLOW) {
                        continue;
                    }
                    WatchEvent<Path> ev = (WatchEvent<Path>) event;
                    Path filename = ev.context();
                    try {
                        // Resolve the filename against the directory.
                        // If the filename is "test" and the directory is "foo",
                        // the resolved name is "test/foo".
                        Path child = directory.resolve(filename);
                        if (!Files.probeContentType(child).startsWith(MIME_TYPE_IMAGE)) {
                            LOGGER.debug("Detected a non image file.");
                            continue;
                        }
                        long lastModified = child.toFile().lastModified();
                        uploadService.uploadImage(session.getSessionId(), activeCharacterId.toString(), lastModified, new FileInputStream(child.toFile()));
                    } catch (IOException e) {
                        LOGGER.error("Error detecting file type.", e);
                    } catch (SessionExpiredException e) {
                        LOGGER.debug("Session expired as an image was about to be uploaded.");
                    }
                }
                boolean valid = watchKey.reset();
                if (!valid) {
                    break;
                }
            } catch (InterruptedException e) {
                LOGGER.debug("Watcher thread was interrupted, while waiting for changes.");
            }
        }
    }

    public void dispose() {
        isDisposed = true;
        fileWatchThread.cancel(true);
        try {
            watchService.close();
        } catch (IOException e) {
            LOGGER.error("Failed to close watch service.", e);
        }
    }
}
