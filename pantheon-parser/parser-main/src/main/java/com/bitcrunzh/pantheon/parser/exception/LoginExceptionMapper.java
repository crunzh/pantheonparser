package com.bitcrunzh.pantheon.parser.exception;

import com.bitcrunzh.portal.api.model.user.LoginException;
import org.apache.cxf.jaxrs.client.ResponseExceptionMapper;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

public class LoginExceptionMapper implements ResponseExceptionMapper<LoginException> {
    @Override
    public LoginException fromResponse(Response r) {
        return new LoginException();
    }
}
