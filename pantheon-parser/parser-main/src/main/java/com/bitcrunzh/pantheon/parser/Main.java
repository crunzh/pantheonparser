package com.bitcrunzh.pantheon.parser;

import com.bitcrunzh.pantheon.parser.settings.ParserConfiguration;
import com.bitcrunzh.portal.api.LoginService;
import com.bitcrunzh.portal.api.UploadService;
import com.bitcrunzh.portal.api.model.GameType;
import com.bitcrunzh.portal.api.model.user.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Starts the parser tool.
 */
public class Main {
    private static final Logger LOGGER = LoggerFactory.getLogger(LogLineHandler.class);
    private static final String EQ_GAME_EXE_FILE_NAME = "eqgame.exe";
    private static final String PANTHEON_EXE_FILE_NAME = "pantheon.exe";
    private static final List<LogLineHandler> logLineHandlers = new ArrayList<>();
    private static final Object shutdownMutex = new Object();
    private static final Pattern LOG_FILE_NAME_PATTERN_EQ = Pattern.compile("eqlog_(\\w*)_(\\w*).txt");
    private static UUID activeCharacterId = null;
    private static ScreenShotListener screenShotListener;

    public static void main(String[] args) {
        try {
            Path installationDirectory = Paths.get(Main.class.getProtectionDomain().getCodeSource().getLocation().toURI()).getParent().getParent();
            ParserConfiguration parserConfiguration = new ParserConfiguration(installationDirectory);
            GameType gameType = detectGameType(parserConfiguration.getGameDirectory());

            PortalConnection portalConnection = new PortalConnection(parserConfiguration);
            portalConnection.addConnectionListener(new PortalConnection.PortalConnectionChangeListener() {
                @Override
                public void connectionClosed() {
                    disposeAllLogLineHandlers();
                    if(screenShotListener != null) {
                        screenShotListener.dispose();
                    }
                }

                @Override
                public void sessionEstablished(Session session, LoginService loginService, UploadService uploadService) {
                    createLogLineHandlers(parserConfiguration, gameType, session, loginService, uploadService);
                    try {
                        screenShotListener = new ScreenShotListener(new File(parserConfiguration.getScreenshotDirectory()), session, uploadService);
                    } catch (IOException e) {
                        LOGGER.error("Failed to initiate screenshot listener.", e);
                    }
                }
            });
            awaitTermination();
        } catch (Throwable e) {
            LOGGER.error("Pantheon Log Parser failed.", e);
        } finally {
            disposeAllLogLineHandlers();
        }
    }

    private static void notifyActiveCharacterChanged(UUID characterId) {
        if (activeCharacterId != null && activeCharacterId.equals(characterId)) {
            return;
        }
        activeCharacterId = characterId;
        if(screenShotListener != null) {
            screenShotListener.setActiveCharacterId(activeCharacterId);
        }
    }

    private static void awaitTermination() {
        synchronized (shutdownMutex) {
            try {
                shutdownMutex.wait();
            } catch (InterruptedException e) {
                LOGGER.error("Main thread interrupted while waiting for shutdown.");
            }
        }
    }

    private static void createLogLineHandlers(ParserConfiguration parserConfiguration, GameType gameType, Session session, LoginService loginService, UploadService uploadService) {
        List<FoundLogFile> logFiles = resolveLogFiles(parserConfiguration, gameType);
        synchronized (logLineHandlers) {
            for (FoundLogFile foundLogFile : logFiles) {
                LogLineHandler logLineHandler = new LogLineHandler(foundLogFile.getLogFile(), (LOGGER::error), uploadService, loginService, gameType, foundLogFile.getCharacterName(), foundLogFile.getServerName(), session, Main::notifyActiveCharacterChanged);
                logLineHandler.start();
                logLineHandlers.add(logLineHandler);
            }
        }
    }

    private static void disposeAllLogLineHandlers() {
        synchronized (logLineHandlers) {
            for (LogLineHandler handler : logLineHandlers) {
                handler.dispose();
            }
            logLineHandlers.clear();
        }
    }

    private static GameType detectGameType(String gameDirectory) {
        File gameDirectoryFile = new File(gameDirectory);
        String[] files = gameDirectoryFile.list((dir, name) -> name.equals(EQ_GAME_EXE_FILE_NAME));
        if (files != null && files.length >= 1) {
            switch (files[0]) {
                case EQ_GAME_EXE_FILE_NAME:
                    return GameType.EverQuest;
                case PANTHEON_EXE_FILE_NAME:
                    return GameType.Pantheon;
            }
        }
        return GameType.Unknown;
    }

    private static List<FoundLogFile> resolveLogFiles(ParserConfiguration configuration, GameType gameType) {
        File logFileDirectory = new File(configuration.getLogFileDirectory());
        if (!logFileDirectory.exists()) {
            throw new IllegalStateException("Log directory did not exist.");
        }
        if (!logFileDirectory.isDirectory()) {
            throw new IllegalStateException("Log directory '" + logFileDirectory + "' was not a directory.");
        }

        switch (gameType) {
            case EverQuest:
                return getEqLogFiles(logFileDirectory);
        }

        throw new IllegalStateException("No log files found in directory '" + logFileDirectory + "', for " + gameType.name() + "'.");
    }

    private static List<FoundLogFile> getEqLogFiles(File logFileDirectory) {
        List<FoundLogFile> foundLogFiles = new ArrayList<>();
        FilenameFilter filenameFilter = (dir, name) -> {
            Matcher matcher = LOG_FILE_NAME_PATTERN_EQ.matcher(name);
            if (matcher.matches()) {
                foundLogFiles.add(new FoundLogFile(matcher.group(1), matcher.group(2), new File(dir, name)));
                return true;
            }
            return false;
        };
        File[] filesInDirectory = logFileDirectory.listFiles(filenameFilter);
        int noFiles = filesInDirectory == null ? 0 : filesInDirectory.length;
        LOGGER.debug(noFiles + " log files was found in directory.");
        return foundLogFiles;
    }

    public static class FoundLogFile {
        private final String characterName;
        private final String serverName;
        private final File logFile;

        FoundLogFile(String characterName, String serverName, File logFile) {
            this.characterName = characterName;
            this.serverName = serverName;
            this.logFile = logFile;
        }

        String getCharacterName() {
            return characterName;
        }

        String getServerName() {
            return serverName;
        }

        public File getLogFile() {
            return logFile;
        }
    }
}
