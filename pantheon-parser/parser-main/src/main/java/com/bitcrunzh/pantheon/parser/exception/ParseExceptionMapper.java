package com.bitcrunzh.pantheon.parser.exception;

import org.apache.cxf.jaxrs.client.ResponseExceptionMapper;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import java.text.ParseException;

public class ParseExceptionMapper implements ResponseExceptionMapper<ParseException> {

    @Override
    public ParseException fromResponse(Response r) {
        return new ParseException(r.getEntity().toString(), 0);
    }
}
