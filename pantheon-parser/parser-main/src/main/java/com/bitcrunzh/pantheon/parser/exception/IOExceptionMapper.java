package com.bitcrunzh.pantheon.parser.exception;

import org.apache.cxf.jaxrs.client.ResponseExceptionMapper;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import java.io.IOException;

public class IOExceptionMapper implements ResponseExceptionMapper<IOException> {
    @Override
    public IOException fromResponse(Response r) {
        return new IOException(r.getEntity().toString());
    }
}
