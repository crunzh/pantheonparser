package com.bitcrunzh.pantheon.parser;

import org.apache.commons.io.input.Tailer;
import org.apache.commons.io.input.TailerListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;

/**
 * Listen for new line events on a specific txt file.
 * Events will be delivered to subscribers.
 */
class LogFileListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(LogLineHandler.class);
    private static final int FILE_CHECK_INTERVAL = 200;
    private final Tailer tailer;

    LogFileListener(final File logFile, final ExceptionListener exceptionListener, boolean readOnlyFromTail, final LogLineConsumer consumer) {
        if (exceptionListener == null) {
            throw new IllegalArgumentException("ExceptionListener may not be null");
        }
        if (logFile == null) {
            throw new IllegalArgumentException("logFile may not be null");
        }
        if (consumer == null) {
            throw new IllegalArgumentException("consumer may not be null");
        }
        this.tailer = new Tailer(logFile, new TailerListener() {
            public void init(Tailer tailer) {
                //Intentionally left empty
            }

            public void fileNotFound() {
                String message = "File not found. " + logFile;
                exceptionListener.notifyException(message, new FileNotFoundException(message));
            }

            public void fileRotated() {

            }

            public void handle(String logLine) {
                try {
                    consumer.consume(logLine);
                } catch (Throwable e) {
                    LOGGER.error("Error consuming log line '" + logLine + "'. ", e);
                }
            }

            public void handle(Exception e) {
                exceptionListener.notifyException("Error while reading log.", e);
            }
        }, FILE_CHECK_INTERVAL, readOnlyFromTail);
    }

    public void start() {
        tailer.run();
    }

    void dispose() {
        tailer.stop();
    }

    public interface ExceptionListener {
        void notifyException(String message, Exception e);
    }

    public interface LogLineConsumer {
        void consume(String line);
    }
}
