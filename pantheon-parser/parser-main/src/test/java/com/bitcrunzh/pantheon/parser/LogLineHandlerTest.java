package com.bitcrunzh.pantheon.parser;

import org.junit.jupiter.api.BeforeEach;

import java.io.File;


class LogLineHandlerTest {
    private File testLogFile;
    private LogLineHandler logLineHandler;

    @BeforeEach
    void setUp() throws Exception {
        testLogFile = new File(getClass().getResource("/Logs/eqlog_Scratzh_agnarr.txt").toURI().getPath());
    }

    private class ExceptionListener implements ExceptionCallback {
        private String message;

        @Override
        public void ExceptionOccurred(String message, Throwable e) {
            this.message = message;
        }
    }

    private void startParser() {
        Thread thread = new Thread(() -> logLineHandler.start());
        thread.start();
    }
}